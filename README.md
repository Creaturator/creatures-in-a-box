#Creatures in a box:
This is my first game, made in Unity for my study program. This game is a collect and combine platformer, where you have to equip creatures wqith abilitys and send them into levels to collect fossils to unlock new abilities. It was written in C# with Unity. Normally, the game is backed up in my university's Gitlab Server.

Story of the game:
You're a mad scientist and have bet that you can not gain all new fossils for your next project with creatures you created. But these body parts are petrified, spread around in various places and split. So your task is to find the fossil parts at one place, contrive your own tactic to collect them. Then create a creature and send it to these places. After you have collected all parts, you are now allowed to recombine and to unpetrify the fossil and use it to get the rest of the fossils.
Will you be smart enough to reach and hand in all fossils? Find out in Creatures in a box.
