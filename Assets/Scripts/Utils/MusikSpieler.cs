﻿using UnityEngine;

namespace Utils {
    public class MusikSpieler : Parent {

        private int lastPlayed;
        private int beforeLastPlayed;

        void Start() {
            DontDestroyOnLoad(gameObject);
            componentName = "MusikSpieler";
            lastPlayed = beforeLastPlayed = bib.musik.Length + 1;
        }

        public void LateUpdate() {
            if (bib.getMainCamera().GetComponents<AudioSource>() == null ||
                bib.getMainCamera().GetComponents<AudioSource>()[0].isPlaying ||
                GameObject.Find("Tutorial Video") != null) return;
            int nowPlaying = Random.Range(0, bib.musik.Length);
            if (lastPlayed == nowPlaying || nowPlaying == beforeLastPlayed) return;
            bib.playSound(nowPlaying, bib.musik);
            beforeLastPlayed = lastPlayed;
            lastPlayed = nowPlaying;
        }
    }
}