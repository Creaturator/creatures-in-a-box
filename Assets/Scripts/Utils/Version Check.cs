﻿using System;
using System.Reflection;
using UnityEngine;

namespace Utils {
    public class Version_Check : Parent{
        public static void printCompilerVersion() {
            Type type =Type.GetType("Mono.Runtime");
            if (type == null) return;
            MethodInfo displayName = type.GetMethod("GetDisplayName",BindingFlags.NonPublic|BindingFlags.Static);
            if(displayName !=null)
                Debug.Log(displayName.Invoke(null,null));
        }

        // ReSharper disable once UnusedMember.Global
        public static void print_CSharpVersion() {
            bib.print(Environment.Version + " + ein paar C# 7.2 Sachen");
        }
    }
}