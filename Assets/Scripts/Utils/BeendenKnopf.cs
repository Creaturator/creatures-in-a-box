﻿using System;
using System.IO;
using TheEditor.UI.ObjektListener;
using UnityEngine;
using Utils.Saving;

namespace Utils {
    public class BeendenKnopf : Parent {

        private void Start() => componentName = "BeendenKnopf";

        private void Update() {
            if (!Input.GetKeyDown(KeyCode.End)) return;
            bib.playRandomSound();
            EndGame();
        }

        public void EndGame() {
            try {
                if (!(gameObject.scene.name.Equals(bib.getLevelSceneName(0))
                      || gameObject.scene.name.Equals(bib.getLevelSceneName(1))
                      || gameObject.scene.name.Equals(bib.getLevelSceneName(2))
                      || gameObject.scene.name.Equals(bib.getLevelSceneName(3)))) {
                    //if (!Application.isEditor)
                        bib.getGameSaver().saveInit(bib.generateFileName());
                    foreach (GameObject button in bib.korperteilButtons) {
                        if (!button.TryGetComponent(out IActivable activable)) continue;
                        activable.deActivate();
                    }
                }
                if (!Application.isEditor && Debug.isDebugBuild) {
                    if (!Directory.Exists(Directory.GetCurrentDirectory() + "/Log"))
                        Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/Log");
                    /*if (!File.Exists(Directory.GetCurrentDirectory() + "/Log/Creatures in a box Log.txt"))
                        File.Copy(Application.consoleLogPath, Directory.GetCurrentDirectory() +
                                                              "/Log/Creatures in a box Log.txt", false);
                    else
                        File.Copy(Application.consoleLogPath,
                            File.Exists(Directory.GetCurrentDirectory() + "/Log/Creatures in a box Log 0.txt")
                                ? Directory.GetCurrentDirectory() + "/Log/Creatures in a box Log " + DateTime.Now.Millisecond + ".txt"
                                : Directory.GetCurrentDirectory() + "/Log/Creatures in a box Log 0.txt", false);*/
                    File.Copy(Application.consoleLogPath,
                        Directory.GetCurrentDirectory() + $"/Log/Creatures in a box Log {DateTime.Now.Hour};" +
                        $"{DateTime.Now.Minute}; {DateTime.Now.Millisecond}.txt");
                }
                else if (!Application.isEditor) {
                    if (!Directory.Exists(bib.getDataDirectory() + "Log"))
                        Directory.CreateDirectory(bib.getDataDirectory() + "/Log");
                    File.Copy(Application.consoleLogPath,
                        bib.getDataDirectory() + $"/Log/Creatures in a box Log {DateTime.Now.Hour};" +
                        $"{DateTime.Now.Minute}; {DateTime.Now.Millisecond}.txt");
                }    
                Application.Quit();
            } catch(FileNotFoundException e) {
                Debug.LogException(e);
            } catch(IOException e) {
                File.CreateText("Please run game in admin mode to enable saving.txt").Close();
                Debug.LogException(e);
            } catch (Exception e) {
                Debug.LogException(e);
            }finally {
                Application.Quit();
            }
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #endif
        }
    }
}