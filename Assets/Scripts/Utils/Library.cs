﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Level;
using Level.Fähigkeiten;
using Level.UI;
using TheEditor;
using TheEditor.UI;
using TheEditor.UI.ObjektListener;
using TheEditor.UI.Reiter;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils.Saving;
using static System.Environment.SpecialFolder;
using Random = System.Random;
using Resources = UnityEngine.Resources;

namespace Utils {
    public class Library : MonoBehaviour {

    //Hauptmenu
        private const string hintergrund = "Hintergrund";

        //Editor Objekte
        private const string container = "Container";
        private const string editorBoden = "Boden";
        private const string plattform = "Plattform";
        private const string korperTeilTag = "Korperteil";
        private const string augenStandardName = "Augen";
        private const string platzierenObj ="PlatzierungText";
        private const string verbrauchtText = "Verwendet";
        private const string maxErlaubtText = "Erlaubt";
        private int platzierenLange;
        public GameObject koerperObj;

        //Teilparents
        private const string wirbelTag = "Wirbel";
		private const string rumpfParent = "Rumpf";
		private const string beinParent = "Beine";
        private const string kopfParent = "Kopf";
        private const string fahigParent = "Fahigkeiten";

        //Stellen
        private const string handStelle = "Handstelle";
        private const string schwanzStelle = "Schwanzstelle";
        private const string snapStelle = "PlatzierungStelle";

        //Pfade
        private const string meshpfad = "Presets/Korperteile/";
        private const string fellpfad = "Materials/Felle/";
        private const string descpfad = "Beschreibungen/";

        //UI
        private const string editorUITag = "EditorUI";
        private const string bakeButton = "BakeKnopf";
        private const string delKnopf = "DeleteKnopf";
        private const string kategorieText = "Kategorie";
        private const string editorUIButtonTag = "EditorUIButton";
        private const string korperteilUITag = "UIKorperteilButton";
        private const string scaleSlider = "ScaleSlider";
        private const string selectedText = "SelectedText";
        private const string inputPlaceholder = "Placeholder";
        private const string nameneingabe = "Nameneingabe";
        private const string hintergrundGrau = "Hintergrund";
        private const string photoKnopf = "Photo";

        //KorperteilButtons
        private const string armButtonName = "Arm";
        private const string kopfButtonName = "KopfKnopf";
        private const string beinButtonName = "Bein";
        private const string korperButtonName = "Korper";
        private const string fellButtonName = "Fell";
        private const string fahigkeitenButtonName = "Sonstiges";
        private const string PfeilGruppe = "Pfeile";
        private const string UndoButton = "UndoKnopf";

    //Level Objekte
        private const string levelBoden = "Boden";
        private const string nestName = "Nest";
        private const string fossilName = "Fossil";
        private const string startPkt = "Startpunkt";
        private const string levelObjectName = "Level";
        private const string FossilSpawnPkt = "FossilSpawnPkt";
        private const string nestTrigger = "NestTrigger";
        private const string fossilTagName = "Fossilien";
        private const string fossilPalme = "FossilPalme";
        private const string mundstelle = "Mundstelle";
        private const string objektTag = "Levelobjekt";
        private const string levelWand = "Walls";
        private const string BergTriggerPräfix = "Bergtrigger";

        //UI
        private const string fossilUI = "FossilUI";
        private const string levelUITag = "LevelUI";
        private const string fossilGesamtText = "Gesamt";
        private const string fossilCountText = "Gesammelt";
        private const string resetButtonName = "ResetButton";
        private const string levelUIButtonTag = "LevelUIButton";
        private const string returnButton = "SwitchButton";

        //Level Complete
        private const string completeText = "CompleteText";
        private const string completeBackground = "Nachricht";
        private const string unlockInfoObj = "PartText";
        private const string weiterKnopf = "WeiterKnopf";
        private Dictionary<string, string> descDict;
        private int unlockDefaultInfoLength;

        //Pickup Nachrichten
        private readonly string pickupNachrichtFall = $"Press {KeyCode.LeftControl} to drop fossil.";
        private readonly string pickupNachrichtPickup = $"Press {KeyCode.LeftControl} to pickup fossil.";
        private readonly string schuttelNachricht = $"Press {KeyCode.LeftControl} to shake the tree";
        private const string pickupNachrichtDisabled = "You found a bug. Congratulations.";
        private const string pickupText = "PickupText";
        private const string pickupBackground = "PickupBackground";

        //Kamera
        private readonly Vector3 levelCameraPos = new Vector3(-40, 18, -11);
        private readonly Quaternion levelCameraRotation = Quaternion.Euler(28.5f, 0, 0);

    //sonstiges / Levelubergreifend
        private bool isStarted;
        private GameObject sceneSetupperObj;
        private GameObject vieh;
        private const string canvas = "Canvas";
        private const string mainCamera = "Main Camera";
        private const string library = "Library Objekt";
        private const string musikSoundSpieler = "Musik Spieler";
        private const string BeendenKnopf = "BeendenKnopf";
        //#if UNITY_STANDALONE
        private Saver saver;
        private readonly string ordner = Environment.GetFolderPath(MyDocuments) + "/my games/Creatures in a box/";
        //#endif
        public bool build;
        private const string kreaturEditor = "koerper";
        private const string kreaturLevel = "Kreatur";
        public string spielerName = "Testplayer"; //TODO: Spielernamen setzen und per Directory.GetDirectories() gespeicherte Namen holen
        public bool shouldScroll = true;

        //Musik & Sounds
        private const string levelMusikPfad = "Music/Level";
        private const string editorMusikPfad = "Music/Editor";
        private const string UITexturenPfad = "Materials/Textures/UI/";
        public AudioClip[] musik;
        public AudioClip[] knopfSounds;

        //Arrays
        private static readonly string[] levelNames =
            {"Hauptmenu", "Tutorial", "Level Preview", "Return Preview", "Start Editor", "The Editor", "Lade Szene", "Test Level"};
        private GameObject[] fossils;
        public Button[] buttons;
        public GameObject[] korperteilButtons;
        public GameObject[] meshes;
        public Material[] materialien;

        private void Awake() {
            isStarted = false;
            selectedLevel = "Test Level"; //Wird uberschrieben im BakeKnopf
            //if (!Application.isEditor)
                saver = new Saver();
        }

        private void Start() {
            DontDestroyOnLoad(gameObject);
            getMainCamera().GetComponents<AudioSource>()[0].loop = false;
            getMainCamera().GetComponents<AudioSource>()[1].loop = false;
            Random rand = new Random();
            gameObject.transform.position = new Vector3((float) (rand.Next(300) + rand.NextDouble()),
                (float) (rand.Next(300) + rand.NextDouble()), (float) (rand.Next(300) + rand.NextDouble()));
            meshes = Resources.LoadAll<GameObject>(meshpfad);

            if (!Directory.Exists(descpfad) || new []{descpfad + getPartParentName(TeilParents.HeadParent),
                descpfad + getPartParentName(TeilParents.LegParent), descpfad + getPartParentName(TeilParents.AbilityParent)}.
                    Any(d => !Directory.Exists(d)))
                return;
            descDict = new Dictionary<string, string>(meshes.Length);
            foreach(GameObject obj in meshes)
                if (File.Exists(descpfad + getPartParentName(obj) + "/" + obj.name + ".txt"))
                    descDict.Add(obj.name, File.ReadAllText(descpfad + getPartParentName(obj) + "/" + obj.name + ".txt"));
        }

        public void countObjects() {
            //Musik zahlen
            musik = Resources.LoadAll<AudioClip>(build || isInMainMenu() ? levelMusikPfad : editorMusikPfad);

            if (getMainCamera().gameObject.scene.name.Equals(getLevelSceneName(6)))
                return;

            //Fossilien & Knopfe zahlen
            Queue<Button> buttonList = new Queue<Button>();
            Queue<GameObject> fossilListe = new Queue<GameObject>();
            Queue<GameObject> korperteilButtonListe = new Queue<GameObject>(64);

            GameObject[] objs = Resources.FindObjectsOfTypeAll<GameObject>();

            foreach (GameObject obj in objs) {
                // ReSharper disable once LocalVariableHidesMember
                string tag = obj.tag;
                if (!obj.activeInHierarchy) continue;
                if ((sceneSetupperObj == null || !sceneSetupperObj.name.Equals("SceneSetupper")) && obj.name.Equals("SceneSetupper"))
                    sceneSetupperObj = obj;
                else if (obj.TryGetComponent(out Fossil _))
                    fossilListe.Enqueue(obj);
                else if (tag.Contains("Button") && !tag.Equals(korperteilUITag) && obj.TryGetComponent(out Button b))
                    buttonList.Enqueue(b);
                else if (tag.Equals(korperteilUITag) && !build)
                    korperteilButtonListe.Enqueue(obj);
                else if (obj.name.Equals(build ? kreaturLevel : kreaturEditor))
                    vieh = obj;
            }
            materialien = new Material[0];
            fossils = fossilListe.ToArray();
            buttons = buttonList.ToArray();
            korperteilButtons = korperteilButtonListe.ToArray();

            if (isInPreview() || isInMainMenu())
                return;

            if(build) {
                for (int i = 0; i < fossilListe.Count; i++)
                    fossils[i].GetComponent<Fossil>().index = i; //TODO: Prüfen, ob index jetzt richtig vergeben wird
                unlockDefaultInfoLength = getUnlockedPartTextObj().text.Length;
                return;
            }

            //if(inEditor)
            if (koerperObj == null)
                koerperObj = vieh;
            if (GameObject.Find(platzierenObj) != null)
                platzierenLange = getPlaceText().text.Length;
            UIUtils.renewButtonArray();
            fossils = new GameObject[0];
            materialien = Resources.LoadAll<Material>(fellpfad);
            korperteilButtons = UIUtils.radixSort(korperteilButtons);
            UIUtils.changeButtonColor(korperteilButtons);
            foreach (GameObject obj in korperteilButtons) {
                if (obj == null) continue;
                if (!obj.TryGetComponent(out IActivable activable)) continue;
                activable.Start();
                if (obj.TryGetComponent(out Button _)){}
                    //button.onClick.AddListener(activable.OnSelected);
            }
            UIUtils.renewButtonArray();
        }

        public Component[] GetAllComponents(GameObject[] objs) {
            List<Component> liste = new List<Component>();
            foreach(GameObject obj in objs)
                liste.AddRange(GetAllComponents(obj));
            return liste.ToArray();
        }

        public IEnumerable<Component> GetAllComponents(GameObject obj) {
            return obj.GetComponents(typeof(Component));
        }

        public void changeComponentStati(GameObject objectParent, bool statusAfter) {
            GameObject[] objs = getAllChilds(objectParent);
            Array.Resize(ref objs, objs.Length + 1);
            objs[objs.Length - 1] = objectParent;
            Component[] comps = GetAllComponents(objs);
            foreach (Component comp in comps) {
                if (!(comp is Behaviour) && !(comp is Collider) && !(comp is Renderer)) continue;
                switch (comp) {
                    case Behaviour behaviour:
                        behaviour.enabled = statusAfter;
                        continue;
                    case Collider coll:
                        coll.enabled = statusAfter;
                        continue;
                    case Renderer rend:
                        rend.enabled = statusAfter;
                        continue;
                }
            }
        }

        public void changeComponentStati(Transform gObject, bool statusAfter) =>
            changeComponentStati(gObject.gameObject, statusAfter);

        // ReSharper disable once UnusedMember.Global
        public GameObject[] getAllChilds(GameObject[] parents) {
            List<GameObject> childListe = new List<GameObject>();
            foreach(GameObject parent in parents)
                childListe.AddRange(getAllChilds(parent));
            return childListe.ToArray();
        }

        public GameObject[] getAllChilds(GameObject parent) => getAllChilds(parent.transform);

        public GameObject[] getAllChilds(Transform parent) {
            List<GameObject> liste = new List<GameObject>();
            liste = getChildsRek(ref liste, parent);
            liste.RemoveAt(0);
            return liste.ToArray();
        }

        public GameObject[] getAllChildsWithComponent(Transform parent, Type componentType) {
            List<GameObject> liste = new List<GameObject>();
            liste = getChildsRek(ref liste, parent, componentType);
            return liste.ToArray();
        }

        public GameObject[] getAllChildsWithComponentDelParent(GameObject parent, Type componentType) {
            List<GameObject> childs = getAllChildsWithComponent(parent.transform, componentType).ToList();
            childs.RemoveAt(0);
            return childs.ToArray();
        }

        public GameObject[] getAllChildsWithComponent(GameObject parent, Type componentType) =>
            getAllChildsWithComponent(parent.transform, componentType);

        private ref List<GameObject> getChildsRek(ref List<GameObject> bisher, Transform obj, [CanBeNull] Type componentType = null) {
            if (componentType == null)
                componentType = typeof(Component);
            if (obj.TryGetComponent(componentType, out _))
                bisher.Add(obj.gameObject);
            for (int i = 0; i < obj.childCount; i++)
                getChildsRek(ref bisher, obj.GetChild(i), componentType);
            return ref bisher;
        }

        // ReSharper disable once UnusedMember.Global
        public string componentToString(Component comp) =>
            comp is Parent parent ? parent.getComponentName() : comp.GetType().FullName;

        public void playSound(int stelle, AudioClip[] sounds = null) {
            if (sounds == null)
                sounds = knopfSounds;
            playSound(sounds[stelle], sounds.Equals(musik));
        }

        [SuppressMessage("ReSharper", "ParameterHidesMember")]
        public void playSound(AudioClip clip, bool musik = false) {
            AudioSource output = musik ? getMainCamera().GetComponents<AudioSource>()[0] :
                getMainCamera().GetComponents<AudioSource>()[1];
            output.clip = clip;
            output.Play();
        }

        public void playRandomSound(AudioClip[] array = null) {
            if (array == null)
                array = knopfSounds;
            int stelle = new Random().Next(array.Length);
            playSound(stelle, array);
        }

        public Sprite loadImage(string pathInResources, float width = -1, float height = -1) {
            Sprite bildSprite = Resources.Load<Sprite>(pathInResources);
            if (bildSprite != null)
                return bildSprite;
            Texture2D textur = Resources.Load<Texture2D>(pathInResources);
            if (textur == null) {
                Debug.LogErrorFormat($"Kann Textur an Pfad {pathInResources} nicht finden.");
                return null;
            }
            if (width < -0.9999999f || height < -0.999999999f) {
                width = textur.width;
                height = textur.height;
            }
            Rect rect = new Rect(0 , 0, width, height);
            return Sprite.Create(textur, rect, rect.center);
        }

        public string generateFileName() => ordner + "Saves/" + spielerName + "/" + selectedLevel + ".creatures";

        private void Update() {
            if (!isStarted)
                isStarted = true;
        }

        public override string ToString() {
            return library;
        }

        public static Library instance() {
            if (GameObject.Find(library) != null) return GameObject.Find(library).GetComponent<Library>();
            GameObject tmp = new GameObject {hideFlags = HideFlags.DontSave, name = "Delete me"};
            SceneManager.LoadScene(levelNames[0], LoadSceneMode.Single);
            return tmp.AddComponent<Library>();
        }

        public new void print(object obj) => MonoBehaviour.print(obj.ToString());

        // ReSharper disable once UnusedMember.Global
        public void printT(object obj) => MonoBehaviour.print(obj.ToString());

//Beginn Getter
    //Hautmenu
        public string getSubMenuBackgroundName() => hintergrund;

        public bool isInMainMenu() => getMainCamera().gameObject.scene.name.Equals(getLevelSceneName(0));

    //Preview
        public bool isInPreview() => GameObject.Find("Minimierbar") != null;

    //Editor
        public bool isInEditor() => !(isInPreview() || isInMainMenu() || build);

        public GameObject getCreatureContainer() => GameObject.Find(container);
        public CreatureEditor getEditor() => Creature.GetComponent<CreatureEditor>();

        public string getBodyPartTag() => korperTeilTag;
        public string getVertebraTag() => wirbelTag;

        public GameObject getPartParent(TeilParents parentTyp) =>
            vieh.transform.Find(getPartParentName(parentTyp)).gameObject;

        public GameObject getPartParent(GameObject toFindParentFrom) =>
            vieh.transform.Find(getPartParentName(toFindParentFrom)).gameObject;

        public string getPartParentName(TeilParents parentTyp) {
            switch (parentTyp) {
                case TeilParents.HeadParent:
                    return kopfParent;
                case TeilParents.TrunkParent:
                    return rumpfParent;
                case TeilParents.LegParent:
                    return beinParent;
                case TeilParents.AbilityParent:
                    return fahigParent;
                default:
                    throw new ArgumentOutOfRangeException(nameof(parentTyp), parentTyp, null);
            }
        }

        public string getPartParentName(GameObject toFindParentFrom) {
            string objName = toFindParentFrom.name.ToLower(CultureInfo.CurrentCulture);
            if (objName.Contains("kopf") || objName.Contains("schnabel") || objName.Contains("gesicht"))
                return getPartParentName(TeilParents.HeadParent);
            if(objName.Contains("bein"))
                return getPartParentName(TeilParents.LegParent);
            if (objName.Contains("Wirbel"))
                return getPartParentName(TeilParents.TrunkParent);
            return !objName.Equals("augen") ? getPartParentName(TeilParents.AbilityParent) : "";
        }

        public Text getPlaceText() => GameObject.Find(platzierenObj).transform.Find("Text").gameObject.GetComponent<Text>();
        public int getNormalPlaceTextLength() => platzierenLange;

        // ReSharper disable UnusedMember.Global
        public string getStandardEyeName() => augenStandardName;

        public string getTriggerName(Stellen stellenTyp) {
            switch(stellenTyp) {
                case Stellen.Handstelle:
                    return handStelle;
                case Stellen.Mundstelle:
                    return mundstelle;
                case Stellen.Schwanzstelle:
                    return schwanzStelle;
                case Stellen.SnapStelle:
                    return snapStelle;
                default:
                    throw new ArgumentOutOfRangeException(nameof(stellenTyp), stellenTyp,
                        $"Der Stellentyp {stellenTyp} existiert nicht.");
            }
        }

        public GameObject getPlattform() => GameObject.Find(plattform);

        //UI
        public string getEditorUITexturePath() => UITexturenPfad + "Editor/";

        public string getEditorUITagName() => editorUITag;

        public BakeKnopf getBakeButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out BakeKnopf _) &&
                                            b.gameObject.name.Equals(bakeButton))?.gameObject.GetComponent<BakeKnopf>();
        public DeleteButton getDeleteButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out DeleteButton _) &&
                                     b.gameObject.name.Equals(delKnopf))?.gameObject.GetComponent<DeleteButton>();
        public GameObject getCategorieTextObject() => GameObject.Find(kategorieText);
        public string getEditorUIButtonTag() => editorUIButtonTag;
        public Undoer getUndoButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out Undoer _) &&
                                     b.gameObject.name.Equals(UndoButton))?.gameObject.GetComponent<Undoer>();
        public GameObject getUIBackgroundArea() => GameObject.Find(hintergrundGrau);
        public Undoer getUndoClass() => getUndoButton();
        public string getMaterialPath() => fellpfad;
        public string getModelPath() => meshpfad;
        public Photografierer getScreenshotButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out Photografierer _) &&
                                                  b.gameObject.name.Equals(photoKnopf))?.gameObject.GetComponent<Photografierer>();
        public Text getUsedAbilityText() => GameObject.Find(verbrauchtText).GetComponent<Text>();
        public Text getAvailableText() => GameObject.Find(maxErlaubtText).GetComponent<Text>();

        //KorperteilButtons
        public KorperteilReiter getArmButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out KorperteilReiter _) &&
                                     b.gameObject.name.Equals(armButtonName))?.gameObject.GetComponent<KorperteilReiter>();
        public KorperteilReiter getLegButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out KorperteilReiter _) &&
                                     b.gameObject.name.Equals(beinButtonName))?.gameObject.GetComponent<KorperteilReiter>();
        public KorperteilReiter getHeadButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out KorperteilReiter _) &&
                                     b.gameObject.name.Equals(kopfButtonName))?.gameObject.GetComponent<KorperteilReiter>();
        public MaterialReiter getCoatButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out MaterialReiter _) &&
                                     b.gameObject.name.Equals(fellButtonName))?.gameObject.GetComponent<MaterialReiter>();
        public MeshReiter getBodyButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out MeshReiter _) &&
                                     b.gameObject.name.Equals(korperButtonName))?.gameObject.GetComponent<MeshReiter>();
        public KorperteilReiter getAbilityButton() =>
            Array.Find(buttons,b => b.gameObject.TryGetComponent(out KorperteilReiter _) &&
                                     b.gameObject.name.Equals(fahigkeitenButtonName))?.gameObject.GetComponent<KorperteilReiter>();
        public GameObject getArrowGroupParent() => GameObject.Find(PfeilGruppe);
        public GameObject getSelectedBodyPartTextObj() => GameObject.Find(selectedText);
        public InputField getNameInputField() => GameObject.Find(nameneingabe).GetComponent<InputField>();
        public Text getInputPlaceholderObject() => GameObject.Find(inputPlaceholder).GetComponent<Text>();
        public Slider getScaleChangeSlider() => GameObject.Find(scaleSlider).GetComponent<Slider>();
        public string getkorperteilUITag() => korperteilUITag;
        public string getDescriptionFilesPath() => descpfad;

        public GameObject[] getEditorMenuButtons() {
            if (build) throw new InvalidOperationException("Only useful in Editor Mode.");
            return korperteilButtons;
        }

    //Level
        public GameObject[] getFossils() => fossils;
        public string getFossilName() => fossilName;
        public GameObject getFossilNest() => GameObject.Find(nestName);
        public GameObject getStartingPoint() => GameObject.Find(startPkt);
        public GameObject getFossilNestTrigger() => GameObject.Find(nestTrigger);
        public string getFossilPalmBlueprintName() => fossilPalme;
        public GameObject getLevelObjectHolder() => GameObject.Find(levelObjectName);
        public Vector3 getNormalLevelCameraPosition() => levelCameraPos;
        public Quaternion getNormalLevelCameraRotation() => levelCameraRotation;
        public GameObject getMouthPlace() => GameObject.Find(mundstelle);
        public GameObject getCollectedFossilSpawnPoint() => GameObject.Find(FossilSpawnPkt);
        public string getLevelObjectName() => levelObjectName;
        public string getLevelObjectTag() => objektTag;
        public GameObject getLevelBoundaries() => GameObject.Find(levelWand);
        public string getHillTriggerPrefix() => BergTriggerPräfix;

        //UI
        /** 1 fuer disabled, 2 fuer pickup, 3 fuer fallen lassen 4 fuer Baum schutteln.
        */
        public string getPickupNachricht(int verlaufstelle) {
            switch (verlaufstelle) {
                case 1: return pickupNachrichtDisabled;
                case 2: return pickupNachrichtPickup;
                case 3: return pickupNachrichtFall;
                case 4: return schuttelNachricht;
                default: return pickupNachrichtDisabled;
            }
        }
        public GameObject getUIFossil() => GameObject.Find(fossilUI);
        public string getLevelUITag() => levelUITag;
        public string getFossilTag() => fossilTagName;
        public ResetKnopf getResetButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out ResetKnopf _) &&
                                     b.gameObject.name.Equals(resetButtonName))?.gameObject.GetComponent<ResetKnopf>();
        public SwitchButton getReturnButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out SwitchButton _) &&
                                     b.gameObject.name.Equals(returnButton))?.gameObject.GetComponent<SwitchButton>();
        public GameObject getAllFossilsUI() => GameObject.Find(fossilGesamtText);
        public GameObject getUIFossilCount() => GameObject.Find(fossilCountText);
        public string getLevelUIButtonTagName() => levelUIButtonTag;

        /**true fuer Text, false fuer Hintergrund.*/
        public GameObject getPickupTextObject(bool text) => text ? GameObject.Find(pickupText) : GameObject.Find(pickupBackground);

        /** true fuer Text, false fuer Hintergrund.*/

        //Level Complete
        public Text getUnlockedPartTextObj() => GameObject.Find(unlockInfoObj).GetComponent<Text>();
        public int getUnlockedPartDefaultTextLength() => unlockDefaultInfoLength;
        public string getCompleteTextObjName() => completeText;
        public bool getPartDesc(GameObject obj, out string desc ) {
            if (descDict != null && descDict.Count == meshes.Length) {
                desc = descDict[obj.name];
                return true;
            }
            desc = "";
            return false;
        }

        public LevelCompleteHandler getContinueButton() =>
            Array.Find(buttons, b => b.gameObject.TryGetComponent(out LevelCompleteHandler _) &&
                                       b.gameObject.name.Equals(weiterKnopf))?.gameObject.GetComponent<LevelCompleteHandler>();

        public GameObject getLevelCompleteObject(bool text) {
            GameObject background = GameObject.Find(completeBackground);
            return text ? background.transform.Find(completeText).gameObject : background;
        }
    // ReSharper restore UnusedMember.Global

    //sonstiges
        public bool inLevel {
            get => build;
            set => build = value;
        }

        public string selectedLevel { get; set; }


        // ReSharper disable once UnusedMember.Global
        public Button[] getUIButtons() => buttons;

        public GameObject Creature {
            get {
                if (vieh == null && !isInPreview())
                    vieh = null;
                return vieh;
            }
            // ReSharper disable once ValueParameterNotUsed
            set {
                if (vieh == null && !isInPreview())
                    countObjects();
            }
        }

        public GameObject getGround() => !build ? GameObject.Find(editorBoden) : GameObject.Find(levelBoden);
        public Saver getGameSaver() => saver;
        public string getDataDirectory() => ordner;
        public BeendenKnopf getEndGameButton() =>
            Array.Find(buttons, bu => bu.gameObject.name.Equals(BeendenKnopf))?.gameObject.GetComponent<BeendenKnopf>();
        public bool isAlreadyBuild() => inLevel;
        public GameObject getCanvas() => GameObject.Find(canvas);
        public Camera getMainCamera() => GameObject.Find(mainCamera).GetComponent<Camera>();

        public SetupScene getSceneSetupper() => sceneSetupperObj.GetComponent<SetupScene>();

        // ReSharper disable once UnusedMember.Global
        public GameObject getSoundsMusicPlayer() => GameObject.Find(musikSoundSpieler);

        /**
         * Returns scenename corresponding to place in array.
         * 0 = Hauptmenu, 1 = Tutorial Szene, 2 = StartPreview, 3 = ReturnPreview, 4 = StartEditor, 5 = TheEditor, 6 = Lade Szene, all others = Levels.
         * Get the Levelname with variable selectedLevel.
         */
        public string getLevelSceneName(int levelID) => levelNames[levelID];
        public string getUITexturePath() => UITexturenPfad;
    }
}