﻿using System;
using System.Collections.Generic;
using TheEditor;
using UnityEngine;

namespace Utils.Saving {
    public class Undoer : Parent {

        private Stack<ActionData> undoStack;

        private void Start() {
            undoStack = new Stack<ActionData>();
            componentName = "Undoer";
        }

        public void AddAction(bool matAndert) {
            if (bib.getEditor().koerperTeilListe.Count > 0)
                AddAction(bib.getEditor().koerperTeilListe[0], false, matAndert);
        }

        public void AddAction(GameObject g, bool links = false,bool matAndert = false) {
            ActionData s = new ActionData(g, matAndert, links);
            undoStack.Push(s);
        }

        public void Undo() {
            if (undoStack.Count <= 0) return;
            undoStack.Pop().Restore();
            bib.playRandomSound(bib.knopfSounds);
        }

        public Stack<ActionData> getActionStack() => undoStack;

        public readonly struct ActionData {
            private readonly GameObject obj;
            private readonly Vector3 pos;
            private readonly Quaternion rot;
            private readonly Vector3 scale;
            private readonly string material;
            private readonly bool matAndert;
            private readonly bool links;
            private readonly bool isFahigkeitenTeil;

            public void Restore() {
                CreatureEditor editor = bib.getEditor();
                if (!matAndert) {
                    obj.transform.position = pos;
                    obj.transform.rotation = rot;
                    obj.transform.localScale = scale;
                    if (!editor.koerperTeilListe.Contains(obj)) {
                        if (links)
                            editor.koerperTeilListe.Insert(0, obj); //TODO: Restore setzt Kreatiren nicht an relativen Punkt, auch nicht bei localPosition
                        else
                            editor.koerperTeilListe.Add(obj);
                    }
                    editor.lastObjClickt = obj;
                    bib.getScaleChangeSlider().value = scale.z;
                    if (isFahigkeitenTeil)
                        bib.getUsedAbilityText().text = (++CreatureEditor.verfugbarZahler).ToString();
                    obj.SetActive(true);
                } else if (!string.IsNullOrEmpty(material)) {
                    ActionData thiss = this;
                    Material mat = Array.Find(bib.materialien, (mate => mate.mainTexture.name.Equals(thiss.material)));
                    foreach (GameObject korperteil in editor.getBodyPartList()) {
                        if (!korperteil.TryGetComponent(out MeshRenderer renderer)) continue;
                        renderer.sharedMaterial = mat;
                        renderer.material = mat;
                    }
                }
            }

            public ActionData(GameObject g, bool matAndert = false, bool links = false) {
                this.matAndert = matAndert;
                isFahigkeitenTeil = bib.getEditor().isFahigkeitenTeil(g);
                obj = g;
                if (!matAndert) {
                    pos = g.transform.position;
                    rot = g.transform.rotation;
                    scale = g.transform.localScale;
                    this.links = links;
                    material = "";
                    return;
                }
                material = g.TryGetComponent(out MeshRenderer renderer) && renderer.material.mainTexture != null
                    ? renderer.material.mainTexture.name : "";
                pos = Vector3.zero;
                rot = Quaternion.identity;
                scale = Vector3.zero;
                this.links = links;
            }
        }
    }
}