using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using Level;
using Newtonsoft.Json;
using TheEditor;
using TheEditor.UI.ObjektListener;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using Random = System.Random;

namespace Utils.Saving{
// ReSharper disable ParameterHidesMember
// ReSharper disable LocalVariableHidesMember
    public class Saver {

        private static SaveData daten;
        protected static Library bib;

        private void setBib() {
            if (bib != null) return;
            bib = Parent.bib;
        }

        public void saveInit(string path) {
            setBib();
            if (daten.werte == null) return;
            if (bib.inLevel) {
                daten.saveLevel(bib.getFossilNest().GetComponent<FossilVerwaltung>().getFossilStati());
                save(daten, path);
            }
            else if (!bib.isInPreview()) {
                Quaternion rot = bib.Creature.transform.rotation;
                bib.Creature.transform.rotation = Quaternion.Euler(0,180, 0);
                daten.saveEditor(bib.getAllChilds(bib.Creature), bib.Creature.transform.GetComponent<MeshRenderer>().material,
                    bib.getEditor().lastObjClickt);
                save(daten, path);
                // ReSharper disable once Unity.InefficientPropertyAccess
                bib.Creature.transform.rotation = rot;
            }
        }

        public void loadInit(string path) {
            try {
                setBib();
                daten = load(path);
                if (bib.inLevel) {
                    bib.getFossilNest().GetComponent<FossilVerwaltung>().loadSave(daten);
                    return;
                }
                CreatureEditor editor = bib.getEditor();
                editor.koerperTeilListe = daten.loadDictionaryToList(bib);
                GameObject[] korperteile = bib.getAllChildsWithComponent(bib.koerperObj, typeof(MeshFilter));
                if (!korperteile.Any(g => g.name.Equals(daten.zuletztAusgewahlt))) return;
                editor.lastObjClickt = korperteile.First(g => g.name.Equals(daten.zuletztAusgewahlt));
                editor.updateText();
            }
            catch (Exception e) {
                Debug.LogException(e);

            }
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private bool save(SaveData daten, string path) {
            setBib();
            try {
                if (!Directory.Exists(bib.getDataDirectory() + "Saves/" + bib.spielerName + "/"))
                    Directory.CreateDirectory(bib.getDataDirectory() + "Saves/" + bib.spielerName + "/");
                if (daten.werte == null) return true;
                string json = JsonConvert.SerializeObject(daten, Formatting.Indented);
                File.WriteAllText(path, json);
            } catch(DirectoryNotFoundException e) {
                Debug.LogException(e);
                return false;
            } catch(IOException e) {
                Debug.LogException(e);
                return false;
            } catch (Exception e) {
                Debug.LogException(e);
                return false;
            }
            return true;
        }

        // ReSharper disable once UnusedMember.Global
        public bool getNewSave(string path) {
            try {
                daten = load(path);
            } catch(Exception e) {
                Debug.LogException(e);
                return false;
            }
            return true;
        }

        public SaveData load(string path) =>
            daten = File.Exists(path) ? JsonConvert.DeserializeObject<SaveData>(File.ReadAllText(path))
                : new SaveData(new Random().Next(300));

        public SaveData? getData() => daten;
    }

        [Serializable]
        public struct SaveData {

            [FormerlySerializedAs("FossilCollected")]
            public bool[] abgegebeneFossilien;

            [FormerlySerializedAs("Materialname")]
            public string materialName;

            [FormerlySerializedAs("LastEditorObjectSelected")]
            public string zuletztAusgewahlt;
            
            public Dictionary<string, Tuple<string, Wrapper3D[]>> werte;

            // ReSharper disable once UnusedParameter.Local
            public SaveData(int i = 0) {
                abgegebeneFossilien = new bool[0];
                materialName = String.Empty;
                zuletztAusgewahlt = String.Empty;
                werte = new Dictionary<string, Tuple<string, Wrapper3D[]>>(0);
            }

            public void saveLevel(bool[] abgegebeneFossilien) => this.abgegebeneFossilien = abgegebeneFossilien;

            public void saveEditor(GameObject[] teile, Material material, GameObject lastGewahlt) {
                if (werte == null)
                    werte = new Dictionary<string, Tuple<string, Wrapper3D[]>>();
                saveArrayToDictionary(teile);
                materialName = material.name.Substring(0, material.name.IndexOf(" (", StringComparison.CurrentCulture));
                zuletztAusgewahlt = lastGewahlt.name;
                if (abgegebeneFossilien == null)
                    abgegebeneFossilien = new bool[0];
            }

            public void saveArrayToDictionary(GameObject[] array) {
            werte.Clear();
            Library bib = Library.instance();
            foreach (GameObject obj in array) {
                if (!saveAble(obj, bib)) continue;
                Transform teil = obj.transform;
                try {
                    if (obj.TryGetComponent(out MeshFilter filter)) {
                        Vector3 rotation = obj.transform.rotation.eulerAngles;
                        string meshName = filter.sharedMesh.name;
                        if (meshName.Contains(" Instance"))
                            meshName = meshName.Substring(0, meshName.IndexOf(" Instance", StringComparison.CurrentCulture));
                        werte.Add(obj.name, new Tuple<string, Wrapper3D[]>(meshName, new[] {Wrapper3D.fromVector3(teil.position),
                            Wrapper3D.fromVector3(teil.localScale), Wrapper3D.fromVector3(rotation.y <= 0.0001f ?
                                new Vector3(rotation.x, 0, rotation.z) : rotation)}));
                        continue;
                    }

                    werte.Add(obj.name, new Tuple<string, Wrapper3D[]>("", new [] {Wrapper3D.fromVector3(teil.position),
                        Wrapper3D.fromVector3(teil.localScale), Wrapper3D.fromVector3(obj.transform.rotation.eulerAngles)}));
                }
                catch (ArgumentException e) {
                    Debug.LogException(e);
                    werte = new Dictionary<string, Tuple<string, Wrapper3D[]>>();
                    return;
                }
            }
            }

            public List<GameObject> loadDictionaryToList(Library bib, Dictionary<string, Tuple<string, Wrapper3D[]>> dict = null) {
                if (dict == null)
                    dict = werte;
                if (dict == null || dict.Count <= 0)
                    return new List<GameObject>();
                List<GameObject> liste = new List<GameObject>();
                string materialName1 = materialName;
                Material mat = Array.Find(bib.materialien, material => material.name.Equals(materialName1));
                foreach (KeyValuePair<string, Tuple<string, Wrapper3D[]>> paar in dict) {
                    GameObject obj;
                    if (paar.Value.Item1.Equals("Cylinder")) {
                        GameObject wirbel = bib.getEditor().addWirbel(paar.Key);
                        Transform trans = wirbel.transform;
                        wirbel.name = paar.Key;
                        trans.position = paar.Value.Item2[0].toVector3();
                        trans.localScale = paar.Value.Item2[1].toVector3();
                        Quaternion transsformRotation = trans.rotation;
                        transsformRotation.eulerAngles = paar.Value.Item2[2].toVector3();
                        trans.parent = bib.getPartParent(TeilParents.TrunkParent).transform;
                        wirbel.tag = bib.getVertebraTag();
                        obj = wirbel;
                        goto Material_Liste;
                    }
                    if (!String.IsNullOrEmpty(paar.Value.Item1)) {
                        obj = Array.Find(bib.meshes.Where(g => g.TryGetComponent(out MeshFilter _)).ToArray(),
                            meshObjekt => meshObjekt.GetComponent<MeshFilter>().sharedMesh.name.Equals(paar.Value.Item1));
                        if (obj == null) {
                            foreach(GameObject meshObjekt in bib.meshes) {
                                if (obj != null)
                                    break;
                                GameObject[] childs = bib.getAllChildsWithComponentDelParent(meshObjekt, typeof(MeshFilter));
                                if (childs.Length > 0 && childs.Any(objectt => objectt.GetComponent<MeshFilter>().sharedMesh.name.Equals(paar.Value.Item1)))
                                    obj = childs.First(objectt => objectt.GetComponent<MeshFilter>().sharedMesh.name.Equals(paar.Value.Item1));
                            }
                            if (obj == null)
                                throw new ArgumentException($"Mesh {paar.Value.Item1} konnte unter allen Meshes aller Presets nicht gefunden werden. Meshes Array hat {bib.meshes.Length} Lange");
                        }
                    }
                    else
                        obj = new GameObject(paar.Key);
                    if (!GameObject.Find(paar.Key))
                        obj = Object.Instantiate(obj);
                    obj.transform.position = paar.Value.Item2[0].toVector3();
                    obj.transform.localScale = paar.Value.Item2[1].toVector3();
                    Quaternion rotation = obj.transform.rotation;
                    rotation.eulerAngles = paar.Value.Item2[2].toVector3();
                    obj.transform.rotation = rotation;
                    obj.tag = bib.getBodyPartTag();
                    obj.transform.SetParent(bib.getPartParent(obj).transform);
                    obj.name = paar.Key;

                    Material_Liste:
                    if (mat != null && obj.TryGetComponent(out MeshRenderer _) && (materialName != "Default-Material"
                                   || materialName != "Standard" ) && !string.IsNullOrEmpty(materialName))
                        MaterialButton.changeMaterials(obj, mat);
                    liste.AddRange(bib.getAllChildsWithComponent(obj, typeof(MeshFilter)));
                }

                if (mat != null) {
                    bib.Creature.GetComponent<MeshRenderer>().sharedMaterial = mat;
                    bib.Creature.GetComponent<MeshRenderer>().material = mat;
                }

                if (!dict.Equals(werte))
                    werte = dict;
                liste = liste.OrderByDescending(g => g.transform.position.z).ToList();
                return liste;
            }

            // ReSharper disable once UnusedMember.Global
            public bool levelComplete() => abgegebeneFossilien.All(gesammelt => gesammelt);

            public override bool Equals(object obj) {
                if (obj == null || !(obj is SaveData other) || werte == null) return false;
                return werte.Equals(other.werte) && werte.Count == other.werte.Count && abgegebeneFossilien.Equals(other.abgegebeneFossilien) && materialName.Equals(other.materialName);
            }

            [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
            public override int GetHashCode() {
                unchecked {
                    var hashCode = (abgegebeneFossilien != null ? abgegebeneFossilien.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (werte != null ? werte.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (materialName != null ? materialName.GetHashCode() : 0);
                    return hashCode;
                }
            }

            public override string ToString() {
                StringBuilder builder = new StringBuilder();
                builder.Append("abgegebene Fossilien: ");
                for (int i = 0; i < abgegebeneFossilien.Length; i++) {
                    builder.Append(", Fossil ");
                    builder.Append(i.ToString());
                    builder.Append(": ");
                    builder.Append(abgegebeneFossilien[i]);
                }
                builder.Append("Material: ");
                builder.Append(materialName);
                builder.Append("\n");
                builder.Append("EditorTeile: ");
                foreach (KeyValuePair<string, Tuple<string, Wrapper3D[]>> editTeil in werte) {
                    builder.Append("\n");
                    builder.Append("Teil ");
                    builder.Append(editTeil.Key);
                    builder.Append(": ");
                    if (editTeil.Value.Item2 != null && editTeil.Value.Item2.Length > 0) {
                        foreach (Wrapper3D wrapper in editTeil.Value.Item2) {
                            builder.Append(wrapper);
                            builder.Append(", ");
                        }
                    }

                    builder.Remove(builder.Length - 2, 2);
                    if (editTeil.Value.Item1 == null) continue;
                    builder.Append(" and Mesh: ");
                    builder.Append(editTeil.Value.Item1);
                    builder.Append(".");
                }
                return builder.ToString();
            }

            private bool saveAble(GameObject obj, Library bib) =>
                !(obj.name.Equals(bib.getPartParentName(TeilParents.TrunkParent)) ||
                obj.name.Equals(bib.getPartParentName(TeilParents.LegParent)) ||
                obj.name.Equals(bib.getPartParentName(TeilParents.HeadParent)) ||
                obj.name.Equals(bib.getPartParentName(TeilParents.AbilityParent)) ||
                obj.name.ToLower().Contains("stelle") || obj.name.ToLower().Contains("augen") ||
                !obj.activeSelf || werte.ContainsKey(obj.name));
        }

        [Serializable]
        public struct Wrapper3D {

            public float x;

            public float y;

            public float z;

            public float[] ubrige;

            public Wrapper3D(params float[] zahlen) { //varargs = String...
                ubrige = new float[0];
                if (zahlen == null) {x = float.MaxValue; y = float.MaxValue; z = float.MaxValue; return;}
                if (zahlen.Length == 0) {x = float.MaxValue; y = float.MaxValue; z = float.MaxValue; return;}
                x = zahlen[0];
                if (zahlen.Length == 1) {y = float.MaxValue; z = float.MaxValue; return;}
                y = zahlen[1];
                if (zahlen.Length == 2) {z = float.MaxValue; return;}
                z = zahlen[2];
                if (zahlen.Length == 3) return;
                    Array.Resize(ref zahlen, zahlen.Length - 3);
                    ubrige = zahlen;
            }

            public static Wrapper3D fromVector3(Vector3 toTake) {
                return new Wrapper3D(toTake.x, toTake.y, toTake.z);
            }

            // ReSharper disable UnusedMember.Global
            public static Wrapper3D fromVector2(Vector2 toTake) {
                return new Wrapper3D(toTake.x, toTake.y);
            }

            public static Wrapper3D fromVector4(Vector4 toTake) {
                return new Wrapper3D(toTake.x, toTake.y, toTake.z, toTake.w);
            }

            public Vector3 toVector3() {
                if (ubrige.Length > 0)
                    Debug.LogWarningFormat("There are more than three coordinates. Deleting {0} elements", ubrige.Length);
                return new Vector3(x, y, z);
            }

            public Vector2 toVector2() {
                if (ubrige.Length > 0 || (float.MaxValue - z) <= 0.00001f)
                    Debug.LogWarningFormat("There are more than three coordinates. Deleting {0} elements", ubrige.Length + 1);
                return new Vector2(x, y);
            }

            public Vector4 toVector4() {
                if (ubrige.Length > 1)
                    Debug.LogWarningFormat("There are more than three coordinates. Deleting {0} elements", ubrige.Length - 4);
                return new Vector4(x, y, z, ubrige[0]);
            }
            // ReSharper enable UnusedMember.Global

            public static Wrapper3D operator +(Wrapper3D eins, Wrapper3D zwei) =>
                new Wrapper3D(eins.x + zwei.x, eins.y + zwei.y, eins.z + zwei.z);

            public static Wrapper3D operator * (Wrapper3D eins, Wrapper3D zwei) =>
                new Wrapper3D(eins.x * zwei.x, eins.y * zwei.y, eins.z * zwei.z);


            public static Wrapper3D operator / (Wrapper3D eins, Wrapper3D zwei) =>
                new Wrapper3D(eins.x / zwei.x, eins.y / zwei.y, eins.z / zwei.z);

            public static Wrapper3D operator - (Wrapper3D eins, Wrapper3D zwei) =>
                new Wrapper3D(eins.x - zwei.x, eins.y - zwei.y, eins.z - zwei.z);

            public override string ToString() {
                StringBuilder builder = new StringBuilder();
                builder.Append("Vector3 Coordinate Wrapper with Coordinates: X: ");
                builder.Append(x);
                builder.Append(", Y: ");
                builder.Append(y);
                builder.Append(", Z: ");
                builder.Append(z);
                if (ubrige == null || ubrige.Length <= 0) {
                    String output = builder.ToString();
                    return output/*.Substring(0, output.Length - 1)*/;
                }
                builder.Append("And other numbers: ");
                foreach (float zahl in ubrige) {
                    builder.Append(zahl);
                    builder.Append(", ");
                }

                return builder.ToString();
            }
        }
    }