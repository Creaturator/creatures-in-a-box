﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.Saving {
    public class Photografierer : Parent{

        private Timer timer;
        private bool defaultLogo = true;
        private string ordner;
        private Tuple<Sprite, Sprite> hakenX;
        private Sprite kamera;
        private Image childImage;

        private void Start() {
            timer = new Timer();
            componentName = "PhotoScript";
            hakenX = new Tuple<Sprite, Sprite>(bib.loadImage(bib.getEditorUITexturePath() + "Haken"),
                bib.loadImage(bib.getEditorUITexturePath() + "X"));
            kamera = bib.loadImage(bib.getEditorUITexturePath() + "Kamera");
            childImage = gameObject.transform.GetChild(0).gameObject.GetComponent<Image>();
            ordner = bib.getDataDirectory() + "Screenshots/";
        }

        public void takeScreenshot() {
            try {
                if(!Directory.Exists(ordner))
                    Directory.CreateDirectory(ordner);
                string dateiName = ordner + "Screenshot From " + DateTime.Now.Day + ";" + DateTime.Now.Month + ";" + DateTime.Now.Year + ", " +
                                   DateTime.Now.Hour + ";" + DateTime.Now.Minute + ";" + DateTime.Now.Second;
                dateiName = File.Exists(dateiName + ".png") ? dateiName + DateTime.Now.Ticks : dateiName;
                byte[] bild = ScreenCapture.CaptureScreenshotAsTexture().EncodeToPNG();
                File.WriteAllBytes(dateiName + ".png", bild);
                childImage.sprite = File.Exists(dateiName + ".png") ? hakenX.Item1 : hakenX.Item2;
                timer.startTimer(2f);
                defaultLogo = false;
            } catch (Exception e) {
                Debug.LogException(e);
                try {
                    childImage.sprite = hakenX.Item2;
                    timer.startTimer(2f);
                    defaultLogo = false;
                } catch (Exception f) {
                    Debug.LogException(f);
                }
            }
            bib.playRandomSound();
        }

        private void LateUpdate() {
            if (defaultLogo || timer.isRunning()) return;
            childImage.sprite = kamera;
            defaultLogo = true;
       }

        public string getScreenshotDirection() => ordner;
    }
}