﻿using System;
using System.Linq;
using Level;
using Level.Fähigkeiten;
using TheEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils.Saving;
using Random = System.Random;

namespace Utils {

    public class SetupScene : Parent {

        public bool toTheEditor;
        private static string lastLoadedScene;
        private Saver saver;

        public void Awake() {
            if (gameObject.scene.GetRootGameObjects().Where(g => g.name.Equals(gameObject.name)).ToArray().Length > 1) {
                DestroyImmediate(gameObject);
                DestroyImmediate(this);
                return;
            }
            DontDestroyOnLoad(this);
            SceneManager.sceneLoaded += OnSceneLoaded;
            componentName = "SetupScene";
        }

        public void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
            if (gameObject == null)
                return;
            gameObject.SetActive(true);

            string sceneName = scene.name;
            GameObject bibClone = GameObject.Find("Delete me");
            if (bibClone != null)
                DestroyImmediate(bibClone);
            if (bib == null)
                setBib();
            if (saver == null)
                saver = bib.getGameSaver();
            bib.inLevel = sceneName.Equals(bib.selectedLevel) && mode.Equals(LoadSceneMode.Single);
            bib.countObjects();
            if (sceneName.Equals(lastLoadedScene) && !bib.inLevel)
                return;
            lastLoadedScene = sceneName;
            if (sceneName.Equals(bib.getLevelSceneName(6)) || sceneName.Equals(bib.getLevelSceneName(1)))
                return;
            if (sceneName.Equals(bib.getLevelSceneName(2)) || sceneName.Equals(bib.getLevelSceneName(3)))
                loadPreview(scene, false);
            else if (sceneName.Equals(bib.selectedLevel) && mode.Equals(LoadSceneMode.Additive) && bib.isInPreview())
                loadPreview(scene, true);
            else if (bib.inLevel && gameObject.activeSelf)
                loadLevel();
            else if (sceneName.Contains(bib.getLevelSceneName(4)) || sceneName.Contains(bib.getLevelSceneName(5)))
                loadEditor(sceneName.Contains(bib.getLevelSceneName(4)), scene);
            else if (sceneName.Equals(bib.getLevelSceneName(0)))
                loadHauptmenu();

            bib.shouldScroll = true;
            gameObject.SetActive(false);
        }

        private void loadEditor(bool start, Scene scene) {
            Random rand = new Random();
            bool Grass = rand.NextDouble() > 0.5f;
            int zahl = rand.Next(1, 5);
            Material grass = Resources.Load<Material>("Materials/Gras/" + (Grass ? "Grass " + zahl :
                "Sand " + zahl));
            bib.getGround().GetComponent<MeshRenderer>().sharedMaterial = grass;
            bib.getGround().GetComponent<MeshRenderer>().material = grass;
            if (!start) {
                if (!toTheEditor) {
                    SceneManager.LoadScene(bib.getLevelSceneName(4));
                    return;
                }
                if (bib.koerperObj == null) return;
                SceneManager.MoveGameObjectToScene(bib.koerperObj, scene);
                bib.koerperObj.SetActive(true);
                toTheEditor = true;
                bib.Creature = null; //Aufruf setCreature, d.h. GameObject.Find(Creature)
            }
            else {
                toTheEditor = true;
                //if (!Application.isEditor)
                    saver.loadInit(bib.generateFileName());
            }
            bib.getEditor().countFahigTeile();
        }

        private void loadPreview(Scene scene, bool loaded) {
            if (loaded)
                goto Weiter;
            SceneManager.LoadScene(bib.selectedLevel, LoadSceneMode.Additive);
            if (bib.getCreatureContainer() != null && scene.name.Equals(bib.getLevelSceneName(1)))
                Destroy(bib.getCreatureContainer());
            return;

            Weiter:
            if (!scene.name.Equals(bib.selectedLevel) && (!scene.name.Equals(bib.getLevelSceneName(2))
                || !scene.name.Equals(bib.getLevelSceneName(3)))) {
                SceneManager.UnloadSceneAsync(scene);
                SceneManager.LoadScene(bib.selectedLevel, LoadSceneMode.Additive);
            }
			//if(!Application.isEditor) {
                saver.load(bib.generateFileName());
                bib.getFossilNest().GetComponent<FossilVerwaltung>().loadSave(saver.getData());
            //}
            GameObject levelOrig = scene.GetRootGameObjects().First(g => g.name.Equals(bib.getLevelObjectName()));
            Transform levelCam = scene.GetRootGameObjects().First(g => g.name.Equals(bib.getMainCamera().gameObject.name)).transform;
            GameObject levelVorschau = Instantiate(levelOrig);
            levelVorschau.hideFlags = HideFlags.DontSave;
            Component[] behavObjs = bib.GetAllComponents(bib.getAllChilds(levelVorschau));
            Transform previewCam = bib.getMainCamera().gameObject.transform;
            previewCam.localPosition = levelCam.localPosition; previewCam.localRotation = levelCam.localRotation;
            foreach(Component comp in behavObjs) {
                if (comp.gameObject.tag.Equals(bib.getFossilTag()))
                    if ((comp is Rigidbody) || ((comp is BoxCollider box) && box.isTrigger))
                        continue;
                if (!comp.gameObject.isStatic && !comp.gameObject.tag.Equals(bib.getFossilTag()))
                    comp.gameObject.isStatic = true;
                if ((comp is Transform) || (comp is Renderer) || (comp is Terrain) || (comp is MeshFilter) || (comp is BoxCollider))
                    continue;
                Destroy(comp);
            }
            levelVorschau.isStatic = true;
            levelVorschau.SetActive(false);
            if (!levelVorschau.scene.name.Equals(bib.getLevelSceneName(1))
                && !levelVorschau.scene.name.Equals(bib.getLevelSceneName(2)))
                SceneManager.MoveGameObjectToScene(levelVorschau, SceneManager.GetSceneAt(0));
            SceneManager.UnloadSceneAsync(bib.selectedLevel);
            levelVorschau.SetActive(true);
            if (!levelVorschau.name.Equals(bib.getLevelObjectName()))
                levelVorschau.name = bib.getLevelObjectName();
        }

        private void loadLevel() {
            toTheEditor = true;
            bib.getAllFossilsUI().GetComponent<Text>().text = bib.getFossils().Length.ToString();
            GameObject container = bib.getCreatureContainer();
            GameObject kreatur = bib.Creature;
            MeshFilter mf = kreatur.GetComponent<MeshFilter>(); MeshFilter cf = container.GetComponent<MeshFilter>();
            MeshRenderer mr = kreatur.GetComponent<MeshRenderer>(); MeshRenderer cr = container.GetComponent<MeshRenderer>();
            MeshCollider mc = kreatur.GetComponent<MeshCollider>(); MeshCollider cc = container.GetComponent<MeshCollider>();
            mf.mesh = cf.mesh;
            mf.sharedMesh = cf.sharedMesh;
            mr.material = cr.material;
            mr.sharedMaterial = cr.sharedMaterial;
            mc.sharedMesh = cc.sharedMesh;
            mc.convex = cc.convex;

            GameObject[] allStellen = bib.getAllChilds(bib.koerperObj).Where(
                    ga => Enum.TryParse(ga.name, out Stellen _)).ToArray();
            Transform stellen = bib.Creature.transform.Find("Stellen");
            //stellen.localPosition = bib.Creature.GetComponent<MeshCollider>().sharedMesh.bounds.center;
            foreach(GameObject stelleOrig in allStellen) {
                GameObject stelle = Instantiate(stelleOrig, null);
                stelle.hideFlags = HideFlags.DontSave;
                if (stelle.scene.name != bib.selectedLevel)
                   SceneManager.MoveGameObjectToScene(stelle, bib.Creature.scene);
                stelle.transform.parent = stellen;
                stelle.transform.localPosition = stelleOrig.transform.position;
                stelle.name = stelle.name.Substring(0, stelle.name.IndexOf('('));
                Collider trigger = stelle.GetComponent<Collider>();
                trigger.enabled = true;
                if (stelle.name.Equals(bib.getTriggerName(Stellen.Schwanzstelle)) && stelle.TryGetComponent(out BoxCollider trigger2))
                    trigger2.size *= 3;
                //stelle.transform.rotation = Quaternion.Euler(stelle.transform.rotation.eulerAngles.x, stelle.transform.rotation.eulerAngles.y + 90, stelle.transform.rotation.eulerAngles.z);

            }
            if (bib.getAllChildsWithComponent(bib.koerperObj.transform.Find(bib.getPartParentName(TeilParents.LegParent))
                , typeof(MeshFilter)).Any(g => g.GetComponent<MeshFilter>().mesh.name.Contains("Froschbein")))
                kreatur.GetComponent<CreatureMovement>().jump *= 2;

            Transform startPkt = bib.getStartingPoint().transform;
            Vector3 startPosition = startPkt.position;
            kreatur.transform.position = new Vector3(startPosition.x + 14.5f, startPosition.y, startPosition.z);
            kreatur.transform.localScale = container.transform.localScale * 0.4f;
            //stellen.localScale == Vector3.one;
            stellen.localScale *= 1.6f;
            stellen.localScale = Vector3.one;
            kreatur.transform.rotation = startPkt.transform.rotation;
            kreatur.GetComponent<MeshRenderer>().enabled = true;

            // ReSharper disable once LocalVariableHidesMember
            GameObject camera = bib.getMainCamera().gameObject;
            if (camera.transform.parent == null || !camera.transform.parent.Equals(kreatur.transform)) {
                // ReSharper disable once LocalVariableHidesMember
                Bounds coll = mc.sharedMesh.bounds;
                camera.transform.position = startPosition;
                camera.transform.parent = kreatur.transform;
                camera.transform.localRotation = bib.getNormalLevelCameraRotation();
                camera.transform.localPosition = new Vector3(coll.center.x, coll.max.y, coll.min.z) +
                                                 3 * coll.size.z * -1 * camera.transform.forward;
                //TODO:x * -rans.forward bewegen fur Zoom
            }
            if (!bib.getResetButton().gameObject.activeSelf)
                bib.getResetButton().gameObject.SetActive(true);
            //  if(!Application.isEditor)
            saver.loadInit(bib.generateFileName());
        }

        private void loadHauptmenu() {
            if (SceneManager.sceneCount <= 1) return;
            for (int i = 0; i < SceneManager.sceneCount; i++) {
                Scene szene = SceneManager.GetSceneAt(i);
                if (!szene.name.Equals(bib.getLevelSceneName(0)))
                        SceneManager.UnloadSceneAsync(szene);
            }
        }
    }
}