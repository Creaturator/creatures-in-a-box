﻿using System;
using UnityEngine;

namespace Utils {
    public class Parent : MonoBehaviour {
        private static Library pBib;
        protected string componentName;

        public Parent() => componentName = "Not set yet";

        public override string ToString() {
            return componentName;
        }

        protected static void setBib() => bib = null;

        public string getComponentName() => componentName;

        public static Library bib {
            get {
                if (pBib != null) {
                    GameObject[] duplikate = Array.FindAll(pBib.gameObject.scene.GetRootGameObjects(),
                        g => g.name.Equals("SceneSetupper"));
                    if (duplikate.Length <= 1) return pBib;
                    for (int i = 1; i < duplikate.Length;i++)
                        DestroyImmediate(duplikate[i]);
                    return pBib;
                }
                setBib();
               return pBib;
            }
            // ReSharper disable once ValueParameterNotUsed
            private set {
                if (pBib != null) return;
                pBib = Library.instance();
            }
        }
    }
}