﻿using Level.UI;
using UnityEngine;
using Utils;

namespace Level {
    public class WasserReset : Parent {
        private void OnTriggerEnter(Collider other) {
            if(!bib.inLevel || !other.gameObject.Equals(bib.Creature))
                return;
            ResetKnopf.ResetLevelStatic();
        }
    }
}