﻿using UnityEngine;
using Utils;

namespace Level.Hindernisse {
    public class BergGravity : Parent {

        private void Start()
        => componentName = "BergGravity";

        private void OnTriggerEnter(Collider other) {
            if (!bib.inLevel || !other.gameObject.Equals(bib.Creature))
                return;
            other.GetComponent<Rigidbody>().freezeRotation = gameObject.name.Equals(bib.getHillTriggerPrefix() + " oben");
        }

        private void OnTriggerExit(Collider other) {
            if (!bib.inLevel || !other.gameObject.Equals(bib.Creature))
                return;
            GameObject vieh = other.gameObject;
            vieh.GetComponent<Rigidbody>().freezeRotation = gameObject.name.Equals(bib.getHillTriggerPrefix() + " Berg");
        }
    }
}