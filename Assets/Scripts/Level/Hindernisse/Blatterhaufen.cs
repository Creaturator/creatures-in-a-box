﻿using Level.Fähigkeiten;
using UnityEngine;
using Utils;

namespace Level.Hindernisse {
    public class Blatterhaufen : Parent {
        private Vector3 verkleinerung;
        private short leben = 3;
        private static AudioClip sound;
        private GameObject fossil;
        private Timer timer;
        private bool cooldown;

        private void Start() {
            componentName = "Blatterhaufen";
            fossil = gameObject.transform.Find(bib.getFossilName())?.gameObject;
            if (fossil != null) {
                bib.changeComponentStati(fossil, false);
                fossil.SetActive(false);
            }
            if (!bib.inLevel) return;
            verkleinerung = transform.localScale / leben;
            if (sound == null)
                sound = Resources.Load<AudioClip>("Sounds/Blatterrascheln");
            timer = new Timer();
        }

        private void OnTriggerEnter(Collider other) {
            if (!bib.inLevel) return;
            if (leben <= 0 || cooldown || (!other.gameObject.name.Equals(bib.getTriggerName(Stellen.Schwanzstelle))))
                return;
            leben--;
            gameObject.transform.localScale -= verkleinerung;
            if (leben == 0) {
                if (fossil != null) {
                    bib.changeComponentStati(fossil, true);
                    fossil.transform.parent = gameObject.transform.parent;
                    fossil.transform.localScale = new Vector3(1,1,1);
                    fossil.GetComponent<Rigidbody>().useGravity = true;
                    fossil.SetActive(true);
                }
                gameObject.GetComponent<Renderer>().enabled = false;
                gameObject.GetComponent<Collider>().enabled = false;
				bib.playSound(sound);
				gameObject.SetActive(false);
				return;
            }
            gameObject.transform.position -= new Vector3(0,0.3f, 0);
            bib.playSound(sound);
            cooldown = true;
            timer.startTimer(0.4f);
        }

        private void Update() {
            if (!bib.inLevel)
                return;
            if (!timer.isRunning() && cooldown)
                cooldown = false;
        }
    }
}