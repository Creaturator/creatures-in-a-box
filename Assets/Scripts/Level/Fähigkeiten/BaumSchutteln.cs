﻿using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Level.Fähigkeiten {
    public class BaumSchutteln : Parent {
        private Text schuttelText;
        private Image schuttelBild;
        private static AudioClip sound;

        private void Start() {
            componentName = "BaumSchutteler von Baum " + gameObject.name;
            if (!bib.inLevel)
                return;
            schuttelText = bib.getPickupTextObject(true)?.GetComponent<Text>();
            schuttelBild = bib.getPickupTextObject(false)?.GetComponent<Image>();
            if (sound == null)
                sound = Resources.Load<AudioClip>("Sounds/Palmeschutteln");
        }

        private void OnTriggerEnter(Collider other) {
            if (!bib.inLevel) return;
            if (schuttelBild == null || schuttelText == null)
                Start();
            if (!other.gameObject.name.Equals(bib.getTriggerName(Stellen.Handstelle)) ||
                (schuttelBild.enabled || schuttelText.enabled) || !schuttelText.text.Equals(bib.getPickupNachricht(1)))
                return;
            schuttelText.text = bib.getPickupNachricht(4);
            schuttelText.enabled = true;
            schuttelBild.enabled = true;
        }

        private void OnTriggerExit(Collider other) {
            if (!bib.inLevel || !other.gameObject.name.Equals(bib.getTriggerName(Stellen.Handstelle)) ||
                (!schuttelBild.enabled && !schuttelText.enabled) || !schuttelText.text.Equals(bib.getPickupNachricht(4)))
                return;
            schuttelText.enabled = false;
            schuttelBild.enabled = false;
            schuttelText.text = bib.getPickupNachricht(1);
        }

        private void OnTriggerStay(Collider other) {
            if (!bib.inLevel || !other.gameObject.name.Equals(bib.getTriggerName(Stellen.Handstelle)) || !Input.GetKeyDown(KeyCode.LeftControl) ||
                !schuttelText.text.Equals(bib.getPickupNachricht(4)))
                return;
            if (gameObject.transform.Find(bib.getFossilName()) != null) {
                GameObject fossil = gameObject.transform.Find(bib.getFossilName())?.gameObject;
                Bounds blattBounds = gameObject.GetComponents<BoxCollider>()[1].bounds;
                if (fossil == null)
                    return;
                fossil.transform.localPosition = blattBounds.extents;
                fossil.GetComponent<Rigidbody>().useGravity = true;
                fossil.GetComponents<BoxCollider>()[0].enabled = true;
                fossil.GetComponents<BoxCollider>()[1].enabled = true;
                fossil.transform.parent = gameObject.transform.parent.parent;
            }
            bib.playSound(sound);
        }
    }
}