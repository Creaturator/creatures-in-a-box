﻿using System.Diagnostics.CodeAnalysis;
using Level.UI;
using UnityEngine;
using Utils;

namespace Level {
    public class CreatureMovement : Parent {
        public int speed;
        public int jump;
		private bool onGround = true;
        private Vector3 lastPos;
        private Vector3 direction;
        private bool knopfDruckt;
        private Vector3 position;

        private void Start() {
            componentName = "CreatureMovement";
            if (!bib.inLevel) return;
            gameObject.GetComponent<Rigidbody>().freezeRotation = false;
            position = gameObject.GetComponent<MeshCollider>().sharedMesh.bounds.center;
        }

        [SuppressMessage("ReSharper", "LocalVariableHidesMember")]
        void Update() {
            if (!bib.inLevel) return;
            if (!knopfDruckt && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.S) ||
                             Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)
                             || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Space))) {
                gameObject.GetComponent<Rigidbody>().freezeRotation = true;
                knopfDruckt = true;
            }
            Vector3 boundsCenter = gameObject.GetComponent<MeshCollider>().bounds.center;
            Transform transform = gameObject.transform;
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                transform.localPosition += speed * Time.deltaTime * new Vector3(transform.forward.x, 0, transform.forward.z);

            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                transform.localPosition += speed * Time.deltaTime * -1 * new Vector3(transform.forward.x, 0, transform.forward.z);

            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                transform.RotateAround(boundsCenter, new Vector3(0, (speed * Time.deltaTime) * -1, 0), 1);
            //TODO: schauen, warum der Pivot vom Vieh im Nivana ist

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                transform.RotateAround(boundsCenter, new Vector3(0, Time.deltaTime * speed, 0), 1);

            if (Input.GetKeyDown(KeyCode.Space) && onGround) {
                Vector3 jumpVec = new Vector3(0, jump, 0);
                GetComponent<Rigidbody>().AddRelativeForce(jumpVec, ForceMode.Impulse);
                onGround = false;
            }

            Vector3 position = gameObject.transform.position;
            direction = (position - lastPos).normalized;
            lastPos = position;

            if(Input.GetKeyDown(KeyCode.R))
                ResetKnopf.ResetLevelStatic();
        }

		public void OnCollisionEnter(Collision other) {
            if (!bib.inLevel) return;
			if (other.gameObject.tag.Equals(bib.getLevelObjectTag()))
                onGround = true;
        }

        public Vector3 getMoveDirection() => direction;
        public Vector3 realPosition {
            get => position;
            // ReSharper disable once ValueParameterNotUsed
            set => position = gameObject.GetComponent<MeshCollider>().sharedMesh.bounds.center;
        }
    }
}