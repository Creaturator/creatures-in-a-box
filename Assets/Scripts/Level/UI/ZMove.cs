﻿using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Level.UI {
    public class ZMove : Parent {
        void Start() {
           componentName = "ZMove";
           Slider sl = gameObject.GetComponent<Slider>();
            sl.minValue = bib.getMainCamera().gameObject.transform.localPosition.z - 50;
            sl.maxValue = sl.minValue + 50;
        }

        public void OnDragged() {
            Vector3 pos = bib.getMainCamera().gameObject.transform.position;
            bib.getMainCamera().gameObject.transform.position = new Vector3(pos.x, pos.y, gameObject.GetComponent<Slider>().value);
        }
    }
}
