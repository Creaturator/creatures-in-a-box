﻿using System;
using TheEditor.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace Level.UI {
    public class LevelCompleteHandler : Parent {

        public GameObject freizuschalten;

        private void Start() => componentName = "Level Complete Handler";

        public void onFinishedLevel(bool complete) {
            Text partText = bib.getUnlockedPartTextObj();
            if (partText.text.Length != bib.getUnlockedPartDefaultTextLength())
                partText.text = partText.text.Substring(0, bib.getUnlockedPartDefaultTextLength());
            partText.text += freizuschalten.name;

            RuntimePreviewGenerator.BackgroundColor = Color.gray;
            Image partAnsicht = GameObject.Find("Teilansicht").GetComponent<Image>();
            Rect rect = partAnsicht.gameObject.GetComponent<RectTransform>().rect;
            partAnsicht.sprite = Sprite.Create(RuntimePreviewGenerator.
                GenerateModelPreview(freizuschalten.transform, Math.Abs(Convert.ToInt32(partAnsicht.minWidth)), Math.Abs(Convert.ToInt32(partAnsicht.minHeight)), true), rect, rect.center);

            if (bib.getPartDesc(freizuschalten, out string desc))
                GameObject.Find("TeilDesc").GetComponent<Text>().text = "Description:\n" + desc;

            foreach(Button button in bib.buttons) {
                if (button.gameObject.Equals(bib.getEndGameButton().gameObject) || button.gameObject.Equals(gameObject))
                    continue;
                bib.changeComponentStati(button.gameObject, false);
            }

            if (complete) {
                Image im = bib.getLevelCompleteObject(false)?.GetComponent<Image>();
                if (im == null) return;
                Text te = im.gameObject.transform.Find(bib.getCompleteTextObjName()).gameObject.GetComponent<Text>();
                im.enabled = true;
                if (te == null) return;
                te.enabled = true;
            }
            bib.changeComponentStati(gameObject, true);
        }

        public void returnToMenu() {
            Text knopfText = gameObject.transform.Find("Text").gameObject.GetComponent<Text>();
            knopfText.text = "Saving";
            bib.getGameSaver().saveInit(bib.generateFileName());
            knopfText.text = "Quitting level";
            SceneManager.LoadScene(bib.getLevelSceneName(0), LoadSceneMode.Single);
        }

    }
}