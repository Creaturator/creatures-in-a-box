﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace Level.UI {
    public class ResetKnopf : Parent {
        public Text buttonText;

        private void Start() {
            componentName = "ResetKnopf";
            if (bib.inLevel)
                gameObject.SetActive(true);
        }

        public static void ResetLevelStatic() =>
            bib.getResetButton().ResetLevel(false);

        public void ResetLevel(bool userInput) {
            if (!bib.inLevel) return;
            if (userInput)
                bib.playRandomSound(bib.knopfSounds);
            // if(!Application.isEditor) {
                buttonText.text = "Saving";
                bib.getGameSaver().saveInit(bib.generateFileName());
            // }
            buttonText.text = "Resetting";
            SceneManager.LoadScene(gameObject.scene.name, LoadSceneMode.Single);
        }
    }
}
