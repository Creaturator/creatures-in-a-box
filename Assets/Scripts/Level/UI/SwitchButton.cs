﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace Level.UI
{
    public class SwitchButton : Parent {
        public Text text;

        private void Start() => componentName = "SwitchButton";

            public void switchToEditor() {
            if (!bib.inLevel || !gameObject.activeSelf) return;
            bib.playRandomSound(bib.knopfSounds);
            //if (!Application.isEditor) {
                text.text = "Saving";
                bib.getGameSaver().saveInit(bib.generateFileName());
           // }
            text.text = "Returning to Editor";
            SceneManager.LoadScene(bib.getLevelSceneName(5), LoadSceneMode.Single);
        }
    }
}