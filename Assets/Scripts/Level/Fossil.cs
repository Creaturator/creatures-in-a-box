﻿using System;
using System.Linq;
using Level.Fähigkeiten;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Level {
    public class Fossil : Parent {
        public int index;
        private Tuple<BoxCollider, BoxCollider> colls;
        private static Text pickupText;
        private static Image pickUpBackground;
        private static FossilVerwaltung nest;

        private bool aufgenommen;
        private bool pickedUp;
        private bool sichtbar;
        private bool abgeworfen;
        private bool imNest;
        private bool aufBoden;

        private void Start() {
            componentName = "Fossil";
            if (!bib.inLevel) return;
            sichtbar = false;
            colls = new Tuple<BoxCollider, BoxCollider>(gameObject.GetComponents<BoxCollider>()[0], gameObject.GetComponents<BoxCollider>()[1]);
            if (nest == null)
                nest = bib.getFossilNest().GetComponent<FossilVerwaltung>();
            if (pickupText == null)
                pickupText = bib.getPickupTextObject(true).GetComponent<Text>();
            if (pickUpBackground == null)
                pickUpBackground = bib.getPickupTextObject(false).GetComponent<Image>();
        }

        private void FixedUpdate() {
            if (!bib.inLevel || !Input.GetKeyDown(KeyCode.LeftControl) || !pickedUp) return;
            gameObject.transform.parent = bib.getLevelObjectHolder().transform;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            colls.Item1.enabled = true;
            nest.carriedFossil = null;
            aufgenommen = true;
            abgeworfen = true;
        }

        private void OnTriggerEnter(Collider other) {
            if (!bib.inLevel || aufgenommen) return;
            if (isTriggert(other.gameObject) && !pickedUp && !imNest) {
                pickUpBackground.enabled = true;
                pickupText.text = bib.getPickupNachricht(2);
                pickupText.enabled = true;
                aufgenommen = true;
            }

            if (!other.gameObject.Equals(nest.gameObject) && (!other.gameObject.tag.Equals(bib.getLevelObjectTag())
                || other.gameObject.Equals(gameObject.transform.parent.gameObject)) && !pickedUp) return;
            pickedUp = false;
            Rigidbody body = gameObject.GetComponent<Rigidbody>();
            if (other.gameObject.Equals(nest.gameObject)) {
                if (colls == null)
                    Start();
                colls.Item2.enabled = false;
                body.velocity = Vector3.zero;
                body.angularVelocity = Vector3.zero;
                aufgenommen = true;
            }
            else {
                colls.Item2.enabled = true;
                aufBoden = true;
            }

            colls.Item1.enabled = true;
            body.velocity = Vector3.zero;
            body.angularVelocity = Vector3.zero;
            aufgenommen = true;
        }

        private void OnTriggerStay(Collider other1) {
            if (!bib.inLevel) return;
            GameObject other = other1.gameObject;

            if (other == null || !isTriggert(other) || pickedUp || !Input.GetKeyDown(KeyCode.LeftControl) || abgeworfen)
                return;
            //if (pickedUp || !Input.GetKeyDown(KeyCode.LeftControl) || abgeworfen) return;
            gameObject.transform.parent = bib.Creature.transform;
            pickedUp = true;
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            gameObject.transform.localPosition = !other.gameObject.Equals(bib.Creature) ?
                other.gameObject.transform.localPosition + new Vector3(0, 0, 0f) :
                bib.getAllChilds(other.gameObject.transform.Find("Stellen")).First(g =>
                    g.name.Contains(bib.getTriggerName(Stellen.Mundstelle))).transform.localPosition;
            nest.carriedFossil = gameObject;
            pickupText.enabled = false;
            pickUpBackground.enabled = false;
            pickupText.text = bib.getPickupNachricht(1);
            aufgenommen = true;
            colls.Item2.enabled = false;
            colls.Item1.enabled = false;
        }

        private void OnTriggerExit(Collider other) {
            if (!bib.inLevel || aufgenommen) return;
            if (pickedUp && isTriggert(other.gameObject)) {
                MeshCollider coll = other.gameObject.transform.parent.parent.gameObject.GetComponent<MeshCollider>();
                // ReSharper disable once Unity.PreferNonAllocApi
                if (Physics.OverlapBox(coll.bounds.center, coll.bounds.extents).Any(o =>
                    o.gameObject.Equals(nest.gameObject))) {
                    colls.Item1.enabled = true;
                    nest.carriedFossil = null;
                    pickedUp = false;
                }
            }
            pickupText.enabled = false;
            pickUpBackground.enabled = false;
            pickupText.text = bib.getPickupNachricht(1);
            aufgenommen = true;
        }

        public void sichtbarMachen() => sichtbar = true;

        public bool isSichtbar() => bib.inLevel && sichtbar;

        private void LateUpdate() {
            if (!bib.inLevel) return;
            aufgenommen = false;
            if (!abgeworfen) return;
            pickedUp = false;
            abgeworfen = false;
        }

        public void einsammeln() {
            imNest = true;
            gameObject.transform.parent = nest.transform;
        }

        private bool isTriggert(GameObject other) =>
            bib.inLevel && (other.name.Contains(bib.getTriggerName(Stellen.Mundstelle))
                            || (other.name.Contains(bib.getTriggerName(Stellen.Handstelle)) &&
                                other.transform.parent.Find(bib.getTriggerName(Stellen.Mundstelle)) != null) || //TODO: immer, wenn mit Hand genommen und keinen Mund dabei, direkt fallen lassen
                            (aufBoden && other.Equals(bib.Creature)));
    }
}