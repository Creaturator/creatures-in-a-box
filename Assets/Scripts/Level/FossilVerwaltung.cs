﻿using System.Linq;
using JetBrains.Annotations;
using Level.Fähigkeiten;
using Level.UI;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Utils.Saving;

namespace Level {
    public class FossilVerwaltung : Parent {

        private bool[] collected;
        private int fossilCounter;
        private GameObject[] fossilien;

        private Text pickupText;
        private Image pickupBackground;

        private bool abgearbeitet;

        public Text fossilViewLinks;
        [CanBeNull] public GameObject carriedFossil;

        private void Start() {
            if (abgearbeitet) return;
            componentName = "FossilVerwaltung";
            if (!bib.inLevel && !bib.isInPreview()) return;
            fossilCounter = 0;
            pickupText = bib.getPickupTextObject(true)?.GetComponent<Text>();
            pickupBackground = bib.getPickupTextObject(false)?.GetComponent<Image>();
            if (pickupText != null) pickupText.text = bib.getPickupNachricht(1);
            fossilien = bib.getFossils();
            collected = new bool[fossilien.Length];
            if (fossilViewLinks == null)
                Debug.LogError("Du hast vergessen, die UI zuzuweisen! Was kannst du eigentlich?!");
            abgearbeitet = true;
        }

        private void OnTriggerEnter(Collider other) {
            if (!bib.inLevel) return;
            // ReSharper disable once EnforceIfStatementBraces
            if (other.gameObject.name.Equals(bib.getFossilName()) && carriedFossil == null)
                collectFossil(other.gameObject);
            else if ((other.gameObject.name.Equals(bib.Creature.name) || other.gameObject.name.Equals(bib.getTriggerName(Stellen.Mundstelle)) ||
                     other.gameObject.name.Equals(bib.getTriggerName(Stellen.Handstelle))) && carriedFossil != null) {
                pickupText.text = bib.getPickupNachricht(3);
                pickupBackground.enabled = true;
                pickupText.enabled = true;
            }
        }

        private void collectFossil(GameObject other) {
            if (!bib.inLevel) return;
            Fossil script = other.gameObject.GetComponent<Fossil>();
            if (collected[script.index]) return;
            fossilCounter++;
            fossilViewLinks.text = fossilCounter.ToString();
            collected[script.index] = true;
            bib.print("Fossil erkannt.");
            pickupText.enabled = false;
            pickupText.text = bib.getPickupNachricht(1);
            pickupBackground.enabled = false;
            script.einsammeln();

            if (fossilCounter != fossilien.Length) return;
            LevelCompleteHandler handler = bib.getContinueButton();
            handler.enabled = true;
            handler.onFinishedLevel(true);
        }

        private void OnTriggerExit(Collider other) {
            if (!bib.inLevel) return;
            if (!pickupText.enabled && !pickupBackground.enabled) return;
            pickupText.enabled = false;
            pickupText.text = bib.getPickupNachricht(1);
            pickupBackground.enabled = false;
        }

        // ReSharper disable once ParameterHidesMember
        public void loadSave(SaveData? save) {
            // ReSharper disable once LocalVariableHidesMember
            bool[] collected = save?.abgegebeneFossilien;
            Start();
            if (collected == null || collected.Length < fossilien.Length)
                collected = new bool[fossilien.Length];
            if (collected.All(sammelt => sammelt == false)) return;
            if (fossilien == null)
                fossilien = bib.getFossils();
            this.collected = collected;
            GameObject[] objInsNest = new GameObject[collected.Where(gesammelt => gesammelt).ToArray().Length];
            fossilCounter = objInsNest.Length;
            int j = 0;
            for (int i = 0; i < fossilien.Length; i++) {
                if (!collected[i]) continue;
                objInsNest[j] = fossilien[i];
                j++;
            }
            fossilViewLinks.text = fossilCounter.ToString();
            Vector3 fossilSpawn = bib.getCollectedFossilSpawnPoint().transform.position;
            int zahler = 0;
            foreach (GameObject fossil in objInsNest) {
                fossil.transform.position = new Vector3(fossilSpawn.x, fossilSpawn.y + zahler, fossilSpawn.z);
                fossil.transform.parent = bib.getLevelObjectHolder().transform;
                fossil.GetComponents<BoxCollider>()[0].enabled = true;
                fossil.GetComponents<BoxCollider>()[1].enabled = false;
                fossil.GetComponent<Rigidbody>().useGravity = true;
                fossil.GetComponent<Rigidbody>().isKinematic = false;
                zahler++;
            }
        }
        public bool[] getFossilStati() => collected;
    }
}