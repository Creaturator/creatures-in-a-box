﻿using System;
using UnityEngine;
using Utils;

namespace Level {
    public class CameraControll : Parent{

        [NonSerialized]
        private Vector3 lastMausPos;

        public float zoomSpeed;

        private void Start() {
            componentName = "Kamera Kontrolle";
            if (!bib.inLevel) return;
            lastMausPos = Input.mousePosition;
            if (Math.Abs(zoomSpeed) < 0.00001f)
                zoomSpeed = 6f;
        }

        private void Update() {
            if(!bib.inLevel || !Input.mousePresent || !bib.shouldScroll)
                return;
            Transform trans = gameObject.transform;
            MeshCollider coll = bib.Creature.GetComponent<MeshCollider>();
            if (Input.GetKeyDown(KeyCode.Mouse2))
                lastMausPos = Input.mousePosition;
            else if (Input.GetKey(KeyCode.Mouse2)) {
                Vector2 mausMovement = (Input.mousePosition - lastMausPos) / 8;
                Vector3 eulerRotVor = trans.localRotation.eulerAngles;
                float z = eulerRotVor.z;
                float x = eulerRotVor.x;
                trans.RotateAround(coll.bounds.center,
                    new Vector3(0, mausMovement.x, 0), 1f);
                Vector3 rot = trans.localRotation.eulerAngles;
                trans.localRotation = Quaternion.Euler(x, rot.y, z);
                lastMausPos = Input.mousePosition;
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0.000001f && Input.GetAxis("Mouse ScrollWheel") > -0.00001f)
                return;
            Vector3 newPos = trans.forward * (Input.GetAxisRaw("Mouse ScrollWheel")* zoomSpeed);
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
                trans.localPosition += newPos;
            else if (Input.GetAxis("Mouse ScrollWheel") < 0 && newPos.y < coll.sharedMesh.bounds.min.y)
                trans.localPosition += newPos;
        }
    }
}