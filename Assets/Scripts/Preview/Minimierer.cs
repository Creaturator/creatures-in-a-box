﻿using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Preview {
    public class Minimierer : Parent{
        public Image pfeil;
        public GameObject zuMinimieren;
        private bool ausgeklappt = true;
        private Sprite runter;
        private Sprite hoch;
        private void Start() {
            if (pfeil == null || zuMinimieren == null || zuMinimieren.Equals(gameObject))
                Debug.LogError("Da fehlt was du Vollhorst, man ej!!!1!!");
            componentName = "Minimierer";
            runter = bib.loadImage(bib.getUITexturePath() + "Minimieren");
            hoch = bib.loadImage(bib.getUITexturePath() + "Ausklappen");
        }

        public void Sichtbarkeit_andern() {
            if (!enabled || pfeil == null || zuMinimieren == null) return;
            bib.changeComponentStati(zuMinimieren, !ausgeklappt);
            ausgeklappt = !ausgeklappt;
            pfeil.sprite = ausgeklappt ? runter : hoch;
            Vector2 anchoredPosition = pfeil.gameObject.GetComponent<RectTransform>().anchoredPosition;
            anchoredPosition.y += ausgeklappt ? -3 : 3;
            pfeil.gameObject.GetComponent<RectTransform>().anchoredPosition = anchoredPosition;

            bib.playRandomSound();
        }
    }
}