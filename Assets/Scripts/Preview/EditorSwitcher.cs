﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace Preview {
    public class EditorSwitcher : Parent {
        private void Start() => componentName = "EditorSwitcher";

        public void SwitchScene(bool toEditor = true) {
            if (!enabled) return;
            if (toEditor) {
                gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Loading Editor";
                SceneManager.LoadScene(bib.getSceneSetupper().toTheEditor ? bib.getLevelSceneName(5) : bib.getLevelSceneName(4));
            }
            else {
                if (!bib.koerperObj.scene.Equals(bib.gameObject.scene)) //TODO: Weiterleitung zum TheEditor vom LevelPreview aus
                    DontDestroyOnLoad(bib.koerperObj);
                bib.koerperObj.SetActive(false);
                gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Loading Level Preview";
                SceneManager.LoadScene(bib.getLevelSceneName(3));
            }
            bib.playRandomSound();
        }
    }
}