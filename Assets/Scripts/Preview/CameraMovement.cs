﻿using System;
using UnityEngine;
using Utils;

namespace Preview {
    public class CameraMovement : Parent {

        public float speed;

        [NonSerialized]
        private Vector3 lastMausPos;

        private void Start() {
            componentName = "CameraMovement";
            lastMausPos = Input.mousePosition;
        }

        private void Update() {

            // ReSharper disable once LocalVariableHidesMember
            float speed = this.speed / 6;
            Vector3 anderung = Vector3.zero;
            Transform trans = gameObject.transform;
            Vector3 forward = trans.forward;
            Vector3 right = trans.right;

            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                // ReSharper disable Unity.InefficientMultiplicationOrder
                anderung += forward * speed / (Input.GetMouseButton(2) ? 2 : 1);
            if(Input.GetKey(KeyCode.DownArrow)|| Input.GetKey(KeyCode.S))
                anderung -= forward * speed / (Input.GetMouseButton(2) ? 2 : 1);
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                anderung += right * speed / (Input.GetMouseButton(2) ? 2 : 1);
            if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                anderung -= right * speed / (Input.GetMouseButton(2) ? 2 : 1);
            // ReSharper restore Unity.InefficientMultiplicationOrder
            if (Input.mousePresent) {
                if (Input.GetAxisRaw("Mouse ScrollWheel") > 0)
                    anderung += new Vector3(0, Input.GetAxisRaw("Mouse ScrollWheel") + speed, 0);
                if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
                    anderung -= new Vector3(0, (Input.GetAxisRaw("Mouse ScrollWheel") * -1) + speed, 0);
                if (Input.GetMouseButtonDown(1) && bib.shouldScroll)
                    bib.shouldScroll = false;
                if (Input.GetMouseButtonUp(1) && !bib.shouldScroll) {
                    bib.shouldScroll = true;
                    lastMausPos = Input.mousePosition;
                }
            }
            if (anderung != Vector3.zero)
                trans.localPosition += anderung;

            if (lastMausPos.Equals(Input.mousePosition) || !Input.mousePresent || !bib.shouldScroll)
                return;
            Vector2 mausMovement = (Input.mousePosition - lastMausPos) / 8;
            float z = trans.localRotation.eulerAngles.z;
            trans.Rotate(new Vector3(-mausMovement.y, mausMovement.x, 0));
            Vector3 rot = trans.localRotation.eulerAngles;
            trans.localRotation = Quaternion.Euler(rot.x, rot.y, z);
            lastMausPos = Input.mousePosition;
        }
    }
}