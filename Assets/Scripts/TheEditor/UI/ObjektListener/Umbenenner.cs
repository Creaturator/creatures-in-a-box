﻿
using System;
using System.Linq;
using UnityEngine.UI;
using Utils;

namespace TheEditor.UI.ObjektListener {
    public class Umbenenner : MeshButton {

        public InputField userInput;
        public Text placeholder;
        private Timer timer;

        public override void Start() {
            base.Start();
            timer = new Timer();
            userInput = bib.getNameInputField();
            placeholder = bib.getInputPlaceholderObject();
        }

        private void Update() {
            if (!enabled) return;
            if (!timer.isRunning() && placeholder.text.Equals("Name already existent"))
                Editor.updateText();
        }

        public void OnStartEdit() {
            if (!enabled) return;
            Editor.shouldRotate = false;
        }

        public void OnEndEdit() {
            if (!enabled || Editor == null || Editor.lastObjClickt == null) return;
            string oldName = Editor.lastObjClickt.name;
            if (Editor.koerperTeilListe.Any(g => g.name.Equals(userInput.text))) {
                userInput.text = String.Empty;
                placeholder.text = "Name already existent";
                timer.startTimer(4.5f);
                Editor.shouldRotate = true;
                return;
            }
            Editor.lastObjClickt.name = userInput.text;
            userInput.text = String.Empty;
            Editor.updateText();
            Editor.shouldRotate = true;
        }

        /*private void OnGUI() {
            if (!enabled) return;
            Event e = Event.current;
            if (e.isKey)
                bib.printT(e.keyCode); //TODO: uberlegen, wie man hiermit Controls eingeben lassen kann.
            switch (e.type) {            //https://docs.unity3d.com/ScriptReference/Event-keyCode.html
                case EventType.MouseDown:    //https://docs.unity3d.com/ScriptReference/Event-current.html
                    bib.printT(e.button);    //https://docs.unity3d.com/ScriptReference/EventType.KeyDown.html
                    break;                    //https://docs.unity3d.com/ScriptReference/Event.html
                case EventType.ScrollWheel:
                    if (Input.GetAxis("Mouse ScrollWheel") > 0) {
                        bib.printT("hochgescrollt");
                        break;
                    }
                    bib.printT("runtergescrollt");
                    break;
            }
        }*/
    }
}