﻿using UnityEngine;
using Utils;

namespace TheEditor.UI.ObjektListener {
    public class EmptyActivable : Parent, IActivable {
        public void Start() => componentName = "Empty Activable Script for " + gameObject.name;

            public void OnSelected() =>  Debug.LogError("Selected Empty Activable. Nothing in here.");

            public void deActivate() =>
                bib.changeComponentStati(gameObject, false);

            public void activate() =>
                bib.changeComponentStati(gameObject, true);

            public GameObject getAttachedGameObject() => gameObject;
    }
}