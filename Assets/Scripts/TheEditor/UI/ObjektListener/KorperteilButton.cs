﻿using System;
using System.Linq;
using TheEditor.UI.Reiter;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils;

namespace TheEditor.UI.ObjektListener {
    public class KorperteilButton : Parent, IPointerEnterHandler, IPointerExitHandler, IActivable {
        public GameObject objekt;

        private Image bild;
        private Texture2D normal;
        private Texture2D dunkel;
        private bool doppelt;
        private Timer timer;
        private bool waitForReset;
        private string beschreibung;
        private TeilParents parent;

        private int zahler;
        private Text platzierText;
        private KorperteilReiter controller;
        private Tuple<Sprite, Sprite> fahigIcons;
        public bool warteAufPlatz;

        public void Start() {
            componentName = "ObjektListener " + gameObject.name.Substring(gameObject.name.IndexOf(' ')) + " fuer Koerperteile";
            setBib();
            platzierText = bib.getPlaceText();
            timer = new Timer();
            waitForReset = false;
            fahigIcons = new Tuple<Sprite, Sprite>(
                bib.loadImage(bib.getEditorUITexturePath() + "Eigenschaften"), bib.loadImage(
                bib.getEditorUITexturePath() + "Eigenschaften rot"));
        }

        public void activate(GameObject auszuwahlen, KorperteilReiter caller) {
            if (enabled) return;
            if (bild == null)
                bild = gameObject.transform.GetChild(0).gameObject.GetComponent<Image>();
            if (!gameObject.TryGetComponent(out BoxCollider2D _))
                gameObject.AddComponent<BoxCollider2D>().size = gameObject.GetComponent<RectTransform>().sizeDelta;
            objekt = auszuwahlen;
            RuntimePreviewGenerator.BackgroundColor = Color.clear;
            normal = RuntimePreviewGenerator.GenerateModelPreview(objekt.transform);

            Mesh mesh = auszuwahlen.GetComponent<MeshFilter>().sharedMesh;
            GameObject objektDunkel = Instantiate(objekt, transform, true);
            objektDunkel.name = auszuwahlen.name + " Dunkel";
            objektDunkel.hideFlags = HideFlags.DontSave;
            objektDunkel.transform.position = new Vector3(500f, 500f, 500f);
            objektDunkel.GetComponent<MeshFilter>().mesh = mesh;
            objektDunkel.GetComponent<MeshFilter>().sharedMesh = mesh;
            objektDunkel.GetComponent<Renderer>().material.color = new Color(0.03f, 0.01f, 0.08f);
            objektDunkel.GetComponent<Renderer>().sharedMaterial.color = new Color(0.03f, 0.01f, 0.08f);
            dunkel = RuntimePreviewGenerator.GenerateModelPreview(objektDunkel.transform);
            Destroy(objektDunkel);

            changeBild(auszuwahlen);
            controller = caller;

            // ReSharper disable once LocalVariableHidesMember
            if (bib.getPartDesc(objekt, out string success))
                beschreibung = success;
            bib.changeComponentStati(gameObject, true);
        }

        public void OnSelected() {
            try{
                if (!enabled) return;
                if (CreatureEditor.maxErlaubt <= CreatureEditor.verfugbarZahler && bib.getEditor().kostenloseObjs.Any(s => objekt.name.Equals(s))) {
                    ColorBlock colorBlock = gameObject.GetComponent<Button>().colors;
                    colorBlock.normalColor = new Color(0.58f, 0.009f, 0.15f);
                    gameObject.GetComponent<Button>().colors = colorBlock;
                    timer.startTimer(2f);
                    waitForReset = true;
                    GameObject.Find("EigenschaftenBild").GetComponent<Image>().sprite =fahigIcons.Item2;
                    return;
                }
                if (doppelt) {
                    doppelt = false;
                    return;
                }
                platzierText.text = platzierText.text.Substring(0, bib.getNormalPlaceTextLength());
                platzierText.text += objekt.name;
                bib.changeComponentStati(platzierText.gameObject.transform.parent, true);
                foreach(KorperteilButton button in controller.getCurrentActiveButtons().Where(obj => obj.TryGetComponent(out KorperteilButton _))) {
                    if (button.Equals(this))
                        continue;
                    button.warteAufPlatz = false;
                }
                warteAufPlatz = true;
                doppelt = true;
                //CreatureEditor.placedPart = true;
                bib.playRandomSound(bib.knopfSounds);
            } catch(Exception e) {
                Debug.LogException(e);
                ColorBlock colorBlock = gameObject.GetComponent<Button>().colors;
                colorBlock.normalColor = new Color(0.58f, 0.001f, 0.15f);
                gameObject.GetComponent<Button>().colors = colorBlock;
                timer.startTimer(2f);
                waitForReset = true;
            }

        }

        private void LateUpdate() {
            if (!timer.isRunning() && waitForReset) {
                waitForReset = false;
                ColorBlock colorBlock = gameObject.GetComponent<Button>().colors;
                colorBlock.normalColor = new Color(0.4f, 0.4f, 0.4f, 1);
                gameObject.GetComponent<Button>().colors = colorBlock;
                if (CreatureEditor.maxErlaubt <= CreatureEditor.verfugbarZahler)
                    GameObject.Find("EigenschaftenBild").GetComponent<Image>().sprite = fahigIcons.Item1;
            }
            if (!warteAufPlatz || !Input.GetMouseButtonDown(0))
                return;
            GameObject target = null;
            Vector3 hitPos = Vector3.zero;
            if (Camera.main == null) return;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction * 10, out RaycastHit hit)) {
                target = hit.collider.gameObject;
                hitPos = hit.point;
            }
            if (target == null || !target.tag.Equals(bib.getBodyPartTag()) && !target.tag.Equals(bib.getVertebraTag()))
                return;
            spawnTeil(hitPos, target);
        }

        private void spawnTeil(Vector3 hitPos, GameObject target) {
            if (bib.inLevel)
                return;
            warteAufPlatz = false;
            CreatureEditor editor = bib.getEditor();
            GameObject teil = Instantiate(objekt, Vector3.zero, objekt.transform.localRotation);
            if (!teil.tag.Equals(bib.getBodyPartTag()))
                teil.tag = bib.getBodyPartTag();
            // ReSharper disable once LocalVariableHidesMember
            teil.transform.SetParent(bib.getPartParent(teil).transform);
            if (!teil.TryGetComponent(out Collider _)) {
                Debug.LogErrorFormat("Teil {0} hat keinen Collider. Was hast du jetzt schon wieder verpennt?", teil);
               // CreatureEditor.placedPart = false;
                return;
            }

            teil.transform.position = hitPos;
            teil.transform.localRotation = Quaternion.LookRotation(target.transform.position - teil.transform.position);

            zahler++;
            int i = 0;
            while(editor.koerperTeilListe.Any(obj => obj.name.Equals(objekt.name + "  " + ++zahler)) && ++i < 80) {}
            teil.name = objekt.name + " " + zahler;
            if (i == 80)
                Debug.LogError("Fehler beim erstellen eines neuen Eintrages in der koerperteilliste fuer " + objekt.name);
            MaterialButton.changeMaterials(teil, editor.lastObjClickt.GetComponent<MeshRenderer>().sharedMaterial);
            foreach (GameObject obj in bib.getAllChildsWithComponent(teil, typeof(MeshFilter)))
                editor.koerperTeilListe.Insert(editor.koerperTeilListe.FindIndex(g => g.name.Equals(target.name)), obj);
            editor.lastObjClickt = teil;
            doppelt = true;
            bib.changeComponentStati(platzierText.gameObject.transform.parent, false);
            if (!editor.kostenloseObjs.Any(b => teil.GetComponent<MeshFilter>().sharedMesh.name.Equals(b)))
                bib.getUsedAbilityText().text = (++CreatureEditor.verfugbarZahler).ToString();
            //teil.transform.RotateAround(stelle.transform.position, Quaternion.LookRotation(stelle.transform.position - target.transform.position).eulerAngles, 1f);
            //CreatureEditor.placedPart = false;
        }

        public void deActivate() {
            bib.changeComponentStati(gameObject, false);
            normal = null;
            dunkel = null;
            objekt = null;
        }

        private void changeBild(bool sollDunkel) {
            Vector2 buttonSize = gameObject.GetComponent<RectTransform>().sizeDelta;
            if (sollDunkel)
                bild.sprite = Sprite.Create(dunkel, new Rect(0, 0, buttonSize.x, buttonSize.y), Vector2.zero);

            bild.sprite = Sprite.Create(normal, new Rect(0, 0, buttonSize.x, buttonSize.y), Vector2.zero);
        }

        public GameObject getAttachedGameObject() {
            return gameObject;
        }

        public string getDescription() => beschreibung;

        public void OnPointerEnter(PointerEventData eventData) {
            if (!enabled) return;
            changeBild(true);
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (!enabled) return;
            changeBild(false);
        }
    }
}