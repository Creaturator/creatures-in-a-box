﻿using UnityEngine;
using Utils;

namespace TheEditor.UI.ObjektListener {
    public class MeshButton : Parent, IActivable {

        public bool rechts;
        protected static CreatureEditor Editor { get; private set; }

        public virtual void Start() {
            Editor = bib.getEditor();
            componentName = gameObject.name + " der Körper UI";
        }

        public virtual void activate() {
            if (enabled) return;
            bib.changeComponentStati(gameObject, true);
        }

        public virtual void OnSelected() {
            if (!enabled) return;
            Editor.addWirbel(rechts, false);
            bib.playRandomSound(bib.knopfSounds);
        }

        public virtual void deActivate() =>
            bib.changeComponentStati(gameObject, false);

        public GameObject getAttachedGameObject() => gameObject;

        protected void setEditor() => Editor = bib.getEditor();
    }
}