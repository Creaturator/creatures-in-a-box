﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace TheEditor.UI.ObjektListener {
    public class ScaleSlider : MeshButton, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler {

        private float? lastValue;
        private bool darfAufrufen;
        public override void Start() {
            base.Start();
            componentName = "ScaleSlider der Körper UI";
        }

        public override void activate() {
            base.activate();
            if (Editor == null)
                setEditor();
            // ReSharper disable once Unity.NoNullPropagation
            lastValue = Editor.lastObjClickt?.transform.localScale.x;
        }

        public void OnPointerDown(PointerEventData eventData) { 
            //bib.getUndoClass().AddAction(Editor.lastObjClickt);
        }

        public void OnSliderMoved() {
            if (!enabled || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Editor.hasPlacedVertebra() || !darfAufrufen) return;
            Vector3 norm = Editor.lastObjClickt.transform.localScale.normalized;
            Vector3 change = bib.getScaleChangeSlider().value * norm;
            if (!Editor.setScale(new []{change.x, change.y, change.z}) && lastValue.HasValue)
                bib.getScaleChangeSlider().value = lastValue.Value;
            else
                lastValue = bib.getScaleChangeSlider().value;
        }

        public void OnSliderMoved(float arg0) => OnSliderMoved();

        public void OnPointerEnter(PointerEventData eventData) => darfAufrufen = true;

        public void OnPointerExit(PointerEventData eventData) => darfAufrufen = false;
    }
}