﻿using UnityEngine;

namespace TheEditor.UI.ObjektListener {
    public interface IActivable {
        void Start();
        void OnSelected();
        void deActivate();
        GameObject getAttachedGameObject();
    }
}