﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace TheEditor.UI.ObjektListener {
    public class MaterialButton : Parent, IActivable {

        public Material MaterialToShow { get; set; }

        public void Start() {
            if (gameObject == null) return;
            componentName = "ObjektListener " + gameObject.name.Substring(gameObject.name.IndexOf(' ')) + " fuer Material " + MaterialToShow.name;
        }

        public static void changeMaterials(GameObject parent, Material toShow) {
            if (toShow == null) {
                Debug.LogErrorFormat($"Material von {parent.name} ist null");
                return;
            }
            if (toShow.name != "Default-Material")
                changeMaterial(parent.GetComponent<MeshRenderer>(), toShow);
            foreach (GameObject obj in bib.getAllChildsWithComponentDelParent(parent, typeof(MeshFilter))) {
                if (toShow.name == "Default-Material")
                    continue;
                changeMaterial(obj.GetComponent<MeshRenderer>(), toShow);
            }
        }

        public static void changeMaterial(MeshRenderer renderer, Material MaterialToShow) {
            Material[] materials = renderer.materials;
            for(int i = 0; i < materials.Length; i++) {
                materials[i] = MaterialToShow;
                materials[i] = MaterialToShow;
            }
            Material[] sMaterials = renderer.sharedMaterials;
            for(int i = 0; i < sMaterials.Length; i++) {
                sMaterials[i] = MaterialToShow;
                sMaterials[i] = MaterialToShow;
            }
            renderer.material = MaterialToShow;
            renderer.sharedMaterial = MaterialToShow;
            renderer.materials = materials;
            renderer.sharedMaterials = sMaterials;
        }

        [SuppressMessage("ReSharper", "LocalVariableHidesMember")]
        public void OnSelected() {
            if (!enabled || bib.inLevel) return;
            //bib.getUndoClass().AddAction( true);
            foreach (GameObject korperteil in bib.getEditor().getBodyPartList()) {
                if (!korperteil.TryGetComponent(out MeshRenderer renderer)) continue;
                changeMaterial(renderer, this.MaterialToShow);
            }
            bib.Creature.GetComponent<MeshRenderer>().sharedMaterial = MaterialToShow;
            bib.Creature.GetComponent<MeshRenderer>().material = MaterialToShow;
            bib.playRandomSound();
        }

        public void activate(Material material) {
            if (enabled || bib.inLevel) return;
            bib.changeComponentStati(gameObject.transform.GetChild(0), true);
            if (gameObject.TryGetComponent(out BoxCollider2D _))
             gameObject.GetComponent<BoxCollider2D>().enabled = false;
            enabled = true;
            this.MaterialToShow = material;
            RuntimePreviewGenerator.BackgroundColor = Color.clear;
            Vector2 buttonSize = gameObject.GetComponent<RectTransform>().sizeDelta;
            gameObject.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = Sprite.Create(
                RuntimePreviewGenerator.GenerateMaterialPreview(this.MaterialToShow, PrimitiveType.Sphere),
                new Rect(0, 0,buttonSize.x, buttonSize.y), Vector2.zero);
            gameObject.transform.GetChild(0).gameObject.GetComponent<Image>().enabled = true;
            gameObject.GetComponent<Image>().enabled = true;
            gameObject.GetComponent<Button>().enabled = true;
        }

        public void deActivate() {
            if (enabled)
                bib.changeComponentStati(gameObject.transform.GetChild(0), false);
            if (gameObject.TryGetComponent(out BoxCollider2D box))
                box.enabled = false;
            Image bild = gameObject.transform.GetChild(0).gameObject.GetComponent<Image>();
            gameObject.GetComponent<Image>().enabled = false;
            gameObject.GetComponent<Button>().enabled = false;
            bild.sprite = null;
            bild.enabled = false;
            enabled = false;
            MaterialToShow = null;
            }

            public GameObject getAttachedGameObject() {
                return gameObject;
            }
    }
}