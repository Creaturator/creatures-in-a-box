﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace TheEditor.UI {
    public class BakeKnopf : Parent {
        private Timer timeOut;
        private Text text;

        private void OnEnable() {
            componentName = "BakeKnopf";
            timeOut = new Timer();
            text = GetComponentInChildren<Text>();
        }

        private void Update() {
            if (!timeOut.isRunning() && text.text.Equals("Please create a creature before pressing"))
                text.text = "If finished, please press";
        }

        public void bake(int levelID) {
            if (bib.isAlreadyBuild() || !gameObject.activeSelf) return;
            bib.playRandomSound();
            GameObject kreatur = bib.Creature;
            if (kreatur.GetComponent<CreatureEditor>().lastObjClickt == null ||
                kreatur.GetComponent<CreatureEditor>().koerperTeilListe == null ||
                kreatur.GetComponent<CreatureEditor>().koerperTeilListe.Count <= 0) {
                text.text = "Please create a creature before pressing";
                timeOut.startTimer(3f);
                return;
            }
            text.text = "Composing Parts";
            foreach (GameObject teil in bib.getAllChilds(bib.Creature))
                if (!teil.activeSelf)
                    Destroy(teil);
            //bib.getUndoClass().getActionStack().Clear();
            kreatur = kreatur.GetComponent<CreatureEditor>().bakeMesh();
            kreatur.GetComponent<MeshRenderer>().UpdateGIMaterials();
            bib.selectedLevel = bib.getLevelSceneName(levelID + 6);
            //if (!Application.isEditor) {
                text.text = "Saving";
                bib.getGameSaver().saveInit(bib.generateFileName());
            //}
            DontDestroyOnLoad(bib.Creature);
            bib.koerperObj = bib.Creature;
            bib.Creature.SetActive(false);
            text.text = "Game saved. Loading level";
            SceneManager.LoadScene(bib.getLevelSceneName(6), LoadSceneMode.Single);
        }
    }
}