﻿using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace TheEditor.UI {
    public class ScrollCheckbox : Parent{

        private bool locked;
        private bool ausgeführt;

        private void Start() {
            Toggle toggle = gameObject.GetComponent<Toggle>();
            toggle.isOn = true;
            locked = false;
            componentName = "Scroll Checkbox";
        }

        public void shouldScroll(bool unteraufruf) {
            Toggle toggle = gameObject.GetComponent<Toggle>();
            bib.shouldScroll = toggle.isOn;

            if (unteraufruf || !Application.isFocused) return;
            bib.playRandomSound();
            locked = !toggle.isOn;
        }

        private void Update() {
            Toggle toggle = gameObject.GetComponent<Toggle>();
            if (!Application.isFocused && !ausgeführt) {
                toggle.isOn = false;
                shouldScroll(true);
                ausgeführt = true;
            }

            else if (gameObject.GetComponent<Toggle>().isOn == false && !locked && ausgeführt) {
                toggle.isOn = true;
                shouldScroll(true);
                ausgeführt = false;
            }
        }
    }
}