﻿using System;
using TheEditor.UI.ObjektListener;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace TheEditor.UI {
    public class DeleteButton : MeshButton {

        private Timer timer;
        private Text deleteText;

        public override void Start() {
            if (bib.isInMainMenu())
                return;
            base.Start();
            rechts = DateTime.UtcNow.Millisecond % 2 == 0;
            timer = new Timer();
            deleteText = gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
        }

        public override void activate() =>
            bib.changeComponentStati(gameObject, true);

        public override void OnSelected() {
            if (!enabled) return;
            bool klappt = Editor.deletePart();
            bib.playRandomSound();
            if (klappt) return;
                timer.startTimer(3f);

        }

        private void Update() {
            if (bib.isInMainMenu())
                return;
            if (deleteText == null)
                Start();
            if (!Input.GetKeyDown(KeyCode.Delete)) goto Text;
            if (Editor.deletePart() || !bib.getDeleteButton().enabled) goto Text;
            timer.startTimer(3f);

            Text:
            if (deleteText.text.Equals("Click here to delete selected Bodypart") && !timer.isRunning())
                return;
            if (deleteText.text.Equals("Failed to delete Bodypart") && timer.isRunning())
                return;
            gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = timer.isRunning()
                ? "Failed to delete Bodypart" : "Click here to delete selected Bodypart";
        }

        public override void deActivate() {
            base.deActivate();
            enabled = true;
        }
    }
}