﻿using System;
using TheEditor.UI.ObjektListener;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Object = UnityEngine.Object;

namespace TheEditor.UI {

	public static class UIUtils {

		private static Library bib = Library.instance();
		private static GameObject[] array = bib.getEditorMenuButtons();

		public static void renewButtonArray() => array = bib.getEditorMenuButtons();

		public static void changeButtonColor(GameObject[] buttons = null, params Color[] farben) {
			if (bib.isAlreadyBuild()) return;
			if (buttons == null)
				buttons = array;
			if (farben == null || farben.Length <= 0)
				farben = new [] {new Color(0.4f, 0.4f, 0.4f, 1), Color.gray, Color.black,
					new Color(0.43f, 0.43f, 0.43f, 0.9f), new Color(0.47f, 0.47f, 0.47f, 1)};
			foreach (GameObject button in buttons) {
				if (button == null) continue;
				ColorBlock buttonColors = button.GetComponent<Button>().colors;
				buttonColors.normalColor = farben[0];
				buttonColors.pressedColor = farben[1];
				buttonColors.disabledColor = farben[2];
				buttonColors.selectedColor = farben[3];
				buttonColors.highlightedColor = farben[4];
				button.GetComponent<Button>().colors = buttonColors;
			}
		}

		public static GameObject[] radixSort(GameObject[] localButtons = null) {
			if (bib.isAlreadyBuild()) return localButtons;
			if (localButtons == null)
				localButtons = array;

			//erste Zahl
			int[] c2 = {0, 0, 8, 16, 24, 32, 40, 48};
			GameObject[] erstesArray = new GameObject[localButtons.Length + 1];
			foreach (GameObject obj in localButtons) {
				if (obj == null) continue;
				string[] NrString = obj.name.Substring(obj.name.Length - 3).Split(' ');
				int.TryParse(NrString[0], out int zahl);
				erstesArray[c2[zahl]] = obj;
				c2[zahl]++;
			}

			//zweite Zahl
			int[] c = {0, 0, 7, 14, 21, 28, 35, 42, 49, 56};
			GameObject[] sortArray = new GameObject[localButtons.Length + 1];
			foreach (GameObject obj in erstesArray) {
				if (obj == null) continue;
				string[] NrString = obj.name.Substring(obj.name.Length - 3).Split(' ');
				int.TryParse(NrString[1], out int zahl);
				sortArray[c[zahl]] = obj;
				c[zahl]++;
			}

			//sortArray[53] = GameObject.Find("Objekt 6 9");
			Array.Resize(ref sortArray, sortArray.Length - 1);
			return sortArray;
		}

		public static N[] changeClass<N>(ref GameObject[] buttons, int anzahlZuAendern = 0) where N : MonoBehaviour, IActivable {
			if (bib.isAlreadyBuild()) return new N[0];
			if (buttons == null)
				buttons = array;
			if (anzahlZuAendern == 0 || anzahlZuAendern > buttons.Length)
				anzahlZuAendern = buttons.Length;
			N[] n_array = new N[anzahlZuAendern];
			for (int i = 0; i < anzahlZuAendern; i++) {
				GameObject button = buttons[i];
				if (button.TryGetComponent(out N element)) {
					n_array[i] = element;
					continue;
				}
				if (button.TryGetComponent(out BoxCollider2D collider)) Object.Destroy(collider);

				foreach (Component comp in bib.GetAllComponents(button))
					if (!(comp is N) && comp is IActivable && comp is MonoBehaviour)
						Object.Destroy(comp);

				button.AddComponent<N>().enabled = false;
				n_array[i] = button.GetComponent<N>();
				if (button.GetComponents<N>().Length <= 1) continue;
				N[] duplikate = button.GetComponents<N>();
				for (int j = duplikate.Length; j > 1; j--) Object.DestroyImmediate(duplikate[j - 1]);
			}
			return n_array;
		}

		public static void moveButton(GameObject button, bool richtung) {
			if (bib.inLevel) return;
			bib.print(button + " in Richtung " + richtung);
			if (button == null) return;
			Vector2 locPos = button.GetComponent<RectTransform>().localPosition;
			if (richtung)
				button.GetComponent<RectTransform>().localPosition += new Vector3(locPos.x + 10, 0);
			else
				button.GetComponent<RectTransform>().localPosition -= new Vector3(locPos.x + 10, 0);
		}
	}
}