﻿
using System;
using TheEditor.UI.ObjektListener;
using UnityEngine;
using UnityEngine.UI;

namespace TheEditor.UI.Reiter {
    public class MeshReiter : KorperteilReiter, IReiter {

        public GameObject[] pfeile;

        [NonSerialized]
        private bool activated;

        public override void Start() {
            activated = false;
            setBib();
            pfeile = bib.getAllChildsWithComponent(bib.getArrowGroupParent(), typeof(Behaviour));
            objekte = pfeile;
            base.Start();
        }

        public override void OnSelect() {
            try {
               removeBefore();
                foreach (GameObject obj in pfeile) {
                    if (obj.TryGetComponent(out Slider slider)) {
                        //slider.onValueChanged.AddListener(obj.GetComponent<MeshButton>().OnSliderMoved);
                        if (bib.getEditor().lastObjClickt != null)
                            slider.value = bib.getEditor().lastObjClickt.transform.localScale.x;
                        else
                            slider.value = 1;
                    }
                    if (obj.TryGetComponent(out MeshButton script))
                        script.activate();
                    if (obj.TryGetComponent(out EmptyActivable empty))
                        empty.activate();
                }
                bib.getCategorieTextObject().GetComponentInChildren<Text>().text = categorieName;
            } catch(Exception e) {
                Debug.LogException(e);
                bib.getCategorieTextObject().GetComponentInChildren<Text>().text = "Please select another categorie";
            }
            //UIUtils.moveButton(gameObject, true);
            lastButtonSelected = this;
            activated = true;
            bib.playRandomSound();
        }

        public GameObject[] getArrowGroup() => pfeile;

        public override bool isActivated() => activated;

        public override void setDeactive() => activated = false;
    }
}