﻿namespace TheEditor.UI.Reiter {

    public interface IReiter {
        void OnSelect();
        void Start();
        void setDeactive();
        bool isActivated();
    }
}