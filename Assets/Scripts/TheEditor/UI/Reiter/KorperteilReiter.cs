﻿using System;
using System.Linq;
using TheEditor.UI.ObjektListener;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace TheEditor.UI.Reiter {
    public class KorperteilReiter : Parent, IReiter {

        public GameObject[] objekte;
        public string categorieName;
        protected GameObject[] allActivable;

        [NonSerialized]
        private KorperteilButton[] currentButtons;

        protected static IReiter lastButtonSelected;

        [NonSerialized]
        private bool activated;

        public virtual void Start() {
            componentName = "MenuReiter " + gameObject.name;
            if (categorieName != null && !categorieName.Equals("")) return;
            Debug.LogError("Du hast vergessen, " + gameObject.name + " einen Kategorienamen zuzuweisen. Wie vergesslich kann man eigentlich sein?!");
            categorieName = "";
            activated = false;
        }

        protected void setButtons(GameObject[] buttons) {
            if (allActivable != null || buttons == null) return;
            GameObject[] alles = new GameObject[buttons.Length];
            Array.Copy(buttons, alles, buttons.Length);
            GameObject[] pfeile = bib.getBodyButton().pfeile;
            int i = alles.Length;
            Array.Resize(ref alles, alles.Length + pfeile.Length);
            foreach(GameObject obj in pfeile.Where(objj => objj != null && objj.TryGetComponent(out IActivable _))) {
                alles[i] = obj;
                i++;
            }
            allActivable = alles.Where(obj => obj != null).ToArray();
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global
        protected virtual void removeBefore() {
            //UIUtils.moveButton(lastButtonSelected,  false);
            setButtons(bib.getEditorMenuButtons());
            foreach (GameObject obj in allActivable.Where(obj => obj.TryGetComponent(out IActivable _))) {
                IActivable activable = obj.GetComponent<IActivable>();
                activable.deActivate();
                if (obj.TryGetComponent(out Button button) && !obj.TryGetComponent(out MeshButton _))
                    button.onClick.RemoveAllListeners();
            }
            lastButtonSelected?.setDeactive();
        }

        public virtual void OnSelect() {
            try {
                removeBefore();
                currentButtons = UIUtils.changeClass<KorperteilButton>(ref bib.korperteilButtons, objekte.Length);

                if (objekte != null && objekte.Length > 0) {
                    for (int i = 0; i < objekte.Length && i < currentButtons.Length; i++) {
                        currentButtons[i].gameObject.GetComponent<Button>().onClick.AddListener(currentButtons[i].OnSelected);
                        currentButtons[i].activate(objekte[i], this);
                    }

                    bib.getCategorieTextObject().GetComponentInChildren<Text>().text = categorieName;
                }
                else {
                    Debug.LogError("keine Objekte zum Anzeigen");
                    bib.getCategorieTextObject().GetComponentInChildren<Text>().text = "Please select another categorie.";
                }

                //UIUtils.moveButton(gameObject, true); http://www.theappguruz.com/blog/using-delegates-and-events-in-unity : Delegates und Events
                lastButtonSelected = this;
                activated = true;
                bib.playRandomSound();
            }
            catch (Exception e) {
                Debug.LogException(e);
                bib.getCategorieTextObject().GetComponentInChildren<Text>().text = "Please select another categorie.";
            }
        }

        public virtual void setDeactive() => activated = false;

        public virtual bool isActivated() => activated;

        public KorperteilButton[] getCurrentActiveButtons() => currentButtons;
    }
}