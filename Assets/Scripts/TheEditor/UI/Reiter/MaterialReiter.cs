﻿using System;
using TheEditor.UI.ObjektListener;
using UnityEngine;
using UnityEngine.UI;

namespace TheEditor.UI.Reiter {
    public class MaterialReiter : KorperteilReiter, IReiter {
        private Material[] materialien;

        [NonSerialized]
        private MaterialButton[] currentButtons;

        [NonSerialized]
        private bool activated;

        public override void Start() {
            materialien = bib.materialien;
            activated = false;
            base.Start();
        }

        public override void OnSelect() {
            try {
                if (materialien == null || materialien.Length <= 0)
                    materialien = bib.materialien;
                removeBefore();

                lastButtonSelected?.setDeactive();

                currentButtons = UIUtils.changeClass<MaterialButton>(ref bib.korperteilButtons, materialien.Length);

                if (materialien != null && materialien.Length > 0) {
                    for (int i = 0; i < materialien.Length && i < currentButtons.Length; i++) {
                        currentButtons[i].gameObject.GetComponent<Button>().onClick.AddListener(currentButtons[i].OnSelected);
                        currentButtons[i].activate(materialien[i]);
                    }

                    bib.getCategorieTextObject().GetComponentInChildren<Text>().text = categorieName;
                }
                else {
                    Debug.LogError("keine Objekte zum Anzeigen");
                    bib.getCategorieTextObject().GetComponentInChildren<Text>().text = "Please select another categorie.";
                }

                // UIUtils.moveButton(gameObject, true);
                lastButtonSelected = this;
                activated = true;
                bib.playRandomSound(bib.knopfSounds);
            }
            catch (Exception e) {
                Debug.LogException(e);
                bib.getCategorieTextObject().GetComponentInChildren<Text>().text = "Please select another categorie.";
            }
        }

        public override void setDeactive() => activated = false;

        public override bool isActivated() => activated;
    }
}