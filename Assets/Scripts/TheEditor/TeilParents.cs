﻿namespace TheEditor {
    public enum TeilParents {
        HeadParent,
        TrunkParent,
        LegParent,
        AbilityParent
    }
}