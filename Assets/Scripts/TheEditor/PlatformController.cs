﻿using UnityEngine;
using Utils;

namespace TheEditor {
    public class PlatformController : Parent {

        private GameObject koerper;

        private void Start() {
            componentName = "PlatformController";
            setBib();
            koerper = bib.Creature;
        }

        private void Update() {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                rotatePlatform(false);

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                rotatePlatform(true);
        }

        private void rotatePlatform(bool rechts) {
            if (!bib.getEditor().shouldRotate) return;
            Transform trans = gameObject.GetComponent<Transform>();
            if (!rechts) {
                float y = Mathf.PingPong(trans.rotation.y, 360);
                if (Mathf.Abs(y) < 1.1f)
                    y = 1;
                else
                    y -= 1;
                gameObject.GetComponent<Transform>().Rotate(new Vector3(0, y, 0), 1.8f, Space.Self);
                koerper.GetComponent<Transform>().RotateAround(trans.position, new Vector3(0, y, 0), 1.8f);
            }
            else {
                float y = Mathf.PingPong(trans.rotation.y, 360);
                if (Mathf.Abs(y) < 1.1f)
                    y = -1;
                else
                    y -= 1;
                GetComponent<Transform>().Rotate(new Vector3(0, y, 0), 1.8f, Space.Self);
                koerper.GetComponent<Transform>().RotateAround(trans.position, new Vector3(0, y, 0), 1.8f);
            }
        }
    }
}