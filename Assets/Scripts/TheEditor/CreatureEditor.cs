﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Random = System.Random;

namespace TheEditor {
    public class CreatureEditor : Parent {

        public List<GameObject> koerperTeilListe;
        public GameObject lastObjClickt;
        public GameObject creatureContainer;
        public bool shouldRotate = true;
        public string[] kostenloseObjs;

        public static int verfugbarZahler;
        public const int maxErlaubt = 4;
        //public static bool placedPart;

        private bool addedWirbel;
        private float lastScale;
        private Transform rumpfParent;
        //private Undoer undoer;
        private bool mouseDragging;
        private bool textChanged;
        private int nummerabzuglich;
        private GameObject koerper;
        private int surpriseZahler;
        private Vector2 lastMausPos;
        private Tuple<GameObject, GameObject> halbkreise;
        private Random rand;

        private void Awake() {
            if (!bib.isInPreview() && !bib.inLevel)
                kostenloseObjs = new[] {"Gesicht", "Bein 1 Instance", "Bein 1", "Augen"};
        }

        private void Start() {
            setBib();
            koerper = bib.Creature;
            nummerabzuglich = 0;
            surpriseZahler = 1;
            //placedPart = false;
            lastMausPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            if (rumpfParent == null)
                rumpfParent = bib.getPartParent(TeilParents.TrunkParent).transform;
            //undoer = bib.getUndoClass();
            componentName = "CreatureEditor";
            if (creatureContainer == null) {
                Debug.LogError("CreatureContainer ist null. Alles muss man selbst machen, Man ej!!");
                creatureContainer = GameObject.Find("Container");
            }
            DontDestroyOnLoad(creatureContainer);
            bib.getAvailableText().text = maxErlaubt.ToString();
            rand = new Random();

            if (koerperTeilListe != null && koerperTeilListe.Count > 0)
                return;

            koerperTeilListe = new List<GameObject>();
            textChanged = true;
            for (int i = 0; i < Math.Round(Math.Pow(Math.PI, Math.E)); i++) {
                addWirbel(i % 2 == 0, true);
                if (GameObject.Find("New Game Object") != null)
                    Destroy(GameObject.Find("New Game Object"));
            }
            /*halbkreise = new Tuple<GameObject, GameObject>(Instantiate(Resources.Load<GameObject>("Presets/Halbkreis")),
                Instantiate(Resources.Load<GameObject>("Presets/Halbkreis")));*/
        }

        // ReSharper disable ParameterHidesMember
        public GameObject addWirbel(string name) {
            if (rumpfParent == null)
             rumpfParent = bib.getPartParent(TeilParents.TrunkParent).transform;
            if (koerperTeilListe == null)
             koerperTeilListe = new List<GameObject>();
            return addWirbel(false, true, name);
        }

        public GameObject addWirbel(bool rechts, bool skipCheck, string name = null) {
            if (!bib.getBodyButton().isActivated() && !skipCheck) return null;
            GameObject neuerWirbel = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            if (lastObjClickt == null)
                lastObjClickt = neuerWirbel;
            neuerWirbel.hideFlags = HideFlags.DontSave;
            if (name != null)
                neuerWirbel.name = name;
            else {
                if (rand.NextDouble() <= 0.2f) {
                    neuerWirbel.name = "Wirbel SURPRISE " + surpriseZahler;
                    surpriseZahler++;
                }
                else
                    neuerWirbel.name = "Wirbel " + (rumpfParent.childCount + 1 - nummerabzuglich);
            }
            neuerWirbel.tag = bib.getVertebraTag();
            Transform trans = neuerWirbel.transform;
            trans.parent = rumpfParent;
            bool erstes = false;
            if (!koerperTeilListe.Exists(teil => teil.transform.parent.gameObject.Equals(rumpfParent.gameObject))) {
                trans.localPosition = new Vector3(0, 0, 0);
                trans.localScale = new Vector3(4f, 0.15f, 4f);
                trans.Rotate(new Vector3(90f, 0f, 0f));
                erstes = true;
            }else {
                neuerWirbel.GetComponent<MeshRenderer>().sharedMaterial =
                    lastObjClickt.GetComponent<MeshRenderer>().sharedMaterial;
            }
            Destroy(neuerWirbel.GetComponent<CapsuleCollider>());
            Destroy(GameObject.Find("New Game Object"));
            MeshCollider coll = neuerWirbel.AddComponent<MeshCollider>();
            coll.cookingOptions = MeshColliderCookingOptions.UseFastMidphase;
            coll.convex = true;

            if (erstes) {
                koerperTeilListe.Add(neuerWirbel);

            }
            else {
                positionWirbel(ref neuerWirbel, rechts);
                if (rechts)
                    koerperTeilListe.Add(neuerWirbel);
                else
                    koerperTeilListe.Insert(0, neuerWirbel);
            }
            lastObjClickt = neuerWirbel;
            bib.getScaleChangeSlider().value = neuerWirbel.transform.localScale.z;
            bib.getScaleChangeSlider().minValue = neuerWirbel.transform.localScale.z;
            bib.getScaleChangeSlider().maxValue = neuerWirbel.transform.localScale.x * 5;
            textChanged = false;
            addedWirbel = true;
            return neuerWirbel;
        }

        private void positionWirbel(ref GameObject wirbel, bool rechts) {
            Transform lastObjTrans;
            Transform wirbelTrans = wirbel.transform;
            Vector3 newPos;
            if (rechts) {
                lastObjTrans = koerperTeilListe.Last(teil => teil.gameObject.transform.parent.gameObject
                                                             == rumpfParent.gameObject).transform;
                newPos = -lastObjTrans.up * 0.3f + lastObjTrans.position;
                wirbelTrans.position = newPos;
                wirbelTrans.rotation = lastObjTrans.rotation;
                wirbelTrans.localScale = lastObjTrans.localScale;
            } else {
                lastObjTrans = koerperTeilListe.First(teil => teil.gameObject.transform.parent.gameObject
                                                              == rumpfParent.gameObject).transform;
                newPos = lastObjTrans.up * 0.3f + lastObjTrans.position;
                wirbelTrans.position = newPos;
                wirbelTrans.rotation = lastObjTrans.rotation;
                wirbelTrans.localScale = lastObjTrans.localScale;
            }
        }

        public GameObject getKlickedBodyPart() {
            if (!bib.shouldScroll) return null;
            GameObject target = null;
            if (Camera.main != null) {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray.origin, ray.direction * 10, out RaycastHit hit))
						target = hit.collider.gameObject;
            }

            if (target == null || !target.tag.Equals(bib.getBodyPartTag()) && !target.tag.Equals(bib.getVertebraTag())) return null;
            textChanged = false;
            bib.getScaleChangeSlider().maxValue = target.transform.localScale.x * 4;
            bib.getScaleChangeSlider().value = target.transform.localScale.z;
            return target;
        }

        public void countFahigTeile() {
            verfugbarZahler = 0;
            verfugbarZahler = koerperTeilListe.Where(isFahigkeitenTeil).ToArray().Length;
            bib.getUsedAbilityText().text = verfugbarZahler.ToString();
        }

        public bool isFahigkeitenTeil(GameObject teil) {
            if (teil.tag.Equals(bib.getVertebraTag()) || !teil.TryGetComponent(out MeshFilter filter))
                return false;
            Mesh sharedMesh = filter.sharedMesh;
            if (sharedMesh.name.Contains(" Instance"))
                filter.sharedMesh.name = sharedMesh.name.Substring(0,
                    sharedMesh.name.IndexOf(" Instance", StringComparison.CurrentCulture));
            bib.print("Korperteil " + filter.sharedMesh.name + " auf Fahigkeit gepruft");
            return !kostenloseObjs.Any(f => filter.sharedMesh.name.Equals(f));
        }

        private void Update() {
            updateScaleClick();
        }

        [SuppressMessage("ReSharper", "BadChildStatementIndent")]
        [SuppressMessage("ReSharper", "Unity.PreferNonAllocApi")]
        private void updateScaleClick() {
            if (Input.GetMouseButtonDown(1))
                lastObjClickt = getKlickedBodyPart();

            if (lastObjClickt == null && textChanged) return;
            if (lastObjClickt == null) {
                bib.getInputPlaceholderObject().GetComponent<Text>().text = "nothing";
                textChanged = true;
                nummerabzuglich = rumpfParent.transform.childCount;
                return;
            }
            if (!textChanged) {
                bib.getInputPlaceholderObject().GetComponent<Text>().text = lastObjClickt.name;
                textChanged = true;
            }

            if (Input.GetAxis("Mouse ScrollWheel") > 0 && bib.shouldScroll)
                changeScale(lastObjClickt.transform.localScale.x * 1/25);
            if (Input.GetAxis("Mouse ScrollWheel") < 0 && lastObjClickt.GetComponent<Transform>().localScale.x >= 0.1f && bib.shouldScroll)
                changeScale(-(lastObjClickt.transform.localScale.x * 1/25));
            if (shouldRotate) { //TODO: nur noch letzten Wirbel drehen lassen, uber Raycast rausbekommen
                if (Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.K) ||
                    Input.GetKeyDown(KeyCode.J) || Input.GetKeyDown(KeyCode.L) ||
                    Input.GetKeyDown(KeyCode.U) || Input.GetKeyDown(KeyCode.O)){}
                    //undoer.AddAction(lastObjClickt);
					//X
                    if (Input.GetKey(KeyCode.I))
                        lastObjClickt.transform.Rotate((float) Math.PI * 0.75f, 0f, 0f, Space.Self);
                    if (Input.GetKey(KeyCode.K))
                        lastObjClickt.transform.Rotate((float) Math.PI * -1 * 0.75f, -0f, -0f, Space.Self);

                    if (!lastObjClickt.tag.Equals(bib.getVertebraTag())) {
                        //Y
                        if (Input.GetKey(KeyCode.U))
                            lastObjClickt.transform.Rotate(-0.0f, (float) Math.PI * 0.75f, 0, Space.Self);
                        if (Input.GetKey(KeyCode.O))
                            lastObjClickt.transform.Rotate(-0.0f, (float) Math.PI * -1 * 0.75f, 0, Space.Self);

                        //Z
                        if (Input.GetKey(KeyCode.J))
                            lastObjClickt.transform.Rotate(0.0f, 0f, (float) Math.PI * 0.75f, Space.Self);
                        if (Input.GetKey(KeyCode.L))
                            lastObjClickt.transform.Rotate(-0.0f, -0f, (float) Math.PI * -1 * 0.75f, Space.Self);
                }
            }

            if (!Input.GetMouseButton(2) || !bib.shouldScroll) {
                mouseDragging = false;
                /*GameObject platte = bib.getPlattform();
                Bounds groundColl = platte.GetComponent<BoxCollider>().bounds;
                if (lastObjClickt.transform.localPosition.y <= -3f || Physics.OverlapBox(groundColl.center,
                    new Vector3(groundColl.extents.x, groundColl.extents.y - 0.25f, groundColl.extents.z)).Length > 1) {
                    Vector3 pos = lastObjClickt.transform.localPosition;
                    lastObjClickt.transform.localPosition = new Vector3(pos.x, 0, pos.z);
                */
            }
            dragDrop();
            lastMausPos = Input.mousePosition;
        }

        public void dragDrop(GameObject toDrag = null) { //TODO: an Wirbeln entlang schieben
            if (!bib.shouldScroll || !Input.GetMouseButton(2)) return;
            Vector2 mausPos = Input.mousePosition;
            if (mouseDragging)
                toDrag = lastObjClickt;
            Rigidbody body = new Rigidbody();
            bool usedGravity = true;
            if (!mouseDragging || toDrag == null) {
                toDrag = getKlickedBodyPart();
                if (mouseDragging && toDrag != null) {
                    goto Drag;
                }
                if (toDrag == null || toDrag.tag.Equals(bib.getVertebraTag())) return;
                lastObjClickt = toDrag;
                //undoer.AddAction(toDrag);
                if (toDrag.TryGetComponent(out Rigidbody bodyTmp)) {
                    body = bodyTmp;
                    usedGravity = bodyTmp.useGravity;
                    bodyTmp.useGravity = false;
                }
            }

            Drag:
            if (Camera.main == null || toDrag == null) return;
            mouseDragging = true;
            Vector2 mausmov = (mausPos - lastMausPos).normalized;
            GameObject wirbel = koerperTeilListe[koerperTeilListe.FindIndex(g => g.name.Equals(toDrag.name)) + 1];
            if (mausmov.x > 0.15f || mausmov.x < -0.15f)
                toDrag.transform.position += new Vector3(0,0, -(mausmov.x) / 3f);
            if (mausmov.y > 0.05f || mausmov.y < -0.05f)
                toDrag.transform.RotateAround(wirbel.transform.position,  new Vector3(0, 0, -mausmov.y), 1f);
            if (Input.GetMouseButton(2)) return;
            if (body == null) {
                mouseDragging = false;
                return;
            }
            body.useGravity = usedGravity;
            mouseDragging = false;
        }

        public void changeScale(float changed) => setScale(new []{lastObjClickt.GetComponent<Transform>().localScale.x + changed,
            lastObjClickt.GetComponent<Transform>().localScale.y + changed, lastObjClickt.GetComponent<Transform>().localScale.z + changed});

        public bool setScale(float[] newValues) {
            try {
                bib.print("Scale called");
                if (lastObjClickt == null || addedWirbel) return false;
                 Bounds groundCollider = bib.getPlattform().GetComponent<BoxCollider>().bounds;
                 Vector3 newSize;
                 // ReSharper disable once Unity.PreferNonAllocApi
                 if(Physics.OverlapBox(groundCollider.center, groundCollider.extents).Any(c => c.gameObject.name.Equals(lastObjClickt.name))
                    && (newValues[0] > lastScale || newValues[0] <= 0))
                     return false;
                 if (lastObjClickt.tag.Equals(bib.getVertebraTag())) {
                     newSize = new Vector3(newValues[0], lastObjClickt.GetComponent<Transform>().localScale.y, newValues[0]);
                     lastObjClickt.GetComponent<Transform>().localScale = newSize;
                     lastScale = newValues[0];
                     return true;
                 }

                 if (!lastObjClickt.tag.Equals(bib.getBodyPartTag()))
                     return false;
                 newSize = new Vector3(newValues[0], newValues[1], newValues[2]);
                 lastObjClickt.GetComponent<Transform>().localScale = newSize;
                 /*Bounds bounds = lastObjClickt.GetComponents<Collider>().bounds;
                 bounds.size = newSize;*/ //TODO: uberlegen, wie man Collider richtig skalliert, wen Teil skalliert und kein MeshCollider.
                 lastScale = newValues[0];
                 return true;
            } catch(Exception e) {
                Debug.LogException(e);
                return false;
            }
        }

        // ReSharper disable once UnusedMember.Global
        public float getRandPosition(GameObject obj, char XYZ) => getRandPosition(obj.transform, XYZ);

        public float getRandPosition(Transform obj, char XYZ) {
            XYZ = new string(new [] {XYZ}).ToLower()[0];
            switch (XYZ) {
                case 'x': return (obj.localPosition + obj.right * (obj.localScale.z / 2f)).x;
                case 'y': return (obj.localPosition + obj.up * (obj.localScale.z / 2f)).y;
                case 'z': return (obj.localPosition + obj.forward * (obj.localScale.y / 2f)).z;
                default: throw new ArgumentException("Unity has only 3 Dimensions.");
            }
        }

        public bool deletePart() {
            try {
                if (lastObjClickt == null || (lastObjClickt.tag.Equals(bib.getVertebraTag()) &&
                    koerperTeilListe.IndexOf(lastObjClickt) != 0 && koerperTeilListe.IndexOf(lastObjClickt)
                    != koerperTeilListe.Count - 1))
                    return false;
                lastObjClickt.SetActive(false);
                //undoer.AddAction(lastObjClickt, koerperTeilListe.IndexOf(lastObjClickt) == 0);
                Vector3 position = lastObjClickt.transform.position;
                lastObjClickt.transform.position = new Vector3(position.x, -10f, position.z); //TODO: zwischen Wirbel und nicht Wirbel unterscheiden
                GameObject obj = lastObjClickt;
                if (koerperTeilListe.IndexOf(lastObjClickt) != 0 && koerperTeilListe[koerperTeilListe.IndexOf(lastObjClickt) - 1] != null) {
                   lastObjClickt = koerperTeilListe[koerperTeilListe.IndexOf(lastObjClickt) - 1];
                   bib.getScaleChangeSlider().value = lastObjClickt.transform.localScale.x;
                }
                else {
                    lastObjClickt = koerperTeilListe.Count > 1 ? koerperTeilListe[1] : null;
                    bib.getScaleChangeSlider().value = lastObjClickt == null ? 1f : lastObjClickt.transform.localScale.x;
                }

                koerperTeilListe.Remove(obj);

                if (isFahigkeitenTeil(obj))
                    bib.getUsedAbilityText().text = (--verfugbarZahler).ToString();
                textChanged = false;
                return true;
            }
            catch (Exception e) {
                Debug.LogException(e);
                return false;
            }
        }

        public GameObject bakeMesh() {
            if (!bib.inLevel && creatureContainer != null) {

                Quaternion rot = koerper.transform.rotation;
                koerper.transform.rotation = Quaternion.Euler(0, 180, 0);

                Mesh mesh = new Mesh {name = "zusammengesetzt"};
                int zahlEinzelMeshes = koerperTeilListe.Where(g => g.TryGetComponent(out MeshFilter _))
                    .Select(g => g.GetComponent<MeshFilter>()).Sum(obj => obj.sharedMesh.subMeshCount);
                CombineInstance[] meshes = new CombineInstance[zahlEinzelMeshes];
                int i = 0;
                foreach (MeshFilter obj in koerperTeilListe.Where(g => g.TryGetComponent(out MeshFilter _))
                    .Select(g => g.GetComponent<MeshFilter>()).Where(f => f.mesh != null)) {
                    if (obj.sharedMesh == null || i >= meshes.Length || !obj.gameObject.activeSelf) continue;
                    for (int j = 0; j < obj.sharedMesh.subMeshCount; j++) {
                        meshes[i] = new CombineInstance {subMeshIndex = j, mesh = obj.sharedMesh,
                            transform = obj.gameObject.GetComponent<Transform>().localToWorldMatrix};
                        i++;
                    }
                }
                mesh.CombineMeshes(meshes);
                mesh.Optimize();

                GameObject finalCreature = creatureContainer;
                finalCreature.GetComponent<MeshFilter>().sharedMesh = mesh;
                finalCreature.GetComponent<MeshFilter>().mesh = mesh;
                finalCreature.GetComponent<MeshRenderer>().sharedMaterial = koerper.transform.GetComponent<MeshRenderer>().sharedMaterial;
                finalCreature.GetComponent<MeshRenderer>().material = koerper.transform.GetComponent<MeshRenderer>().material;
                finalCreature.GetComponent<MeshCollider>().sharedMesh = mesh;
                finalCreature.GetComponent<MeshCollider>().convex = true;
                finalCreature.transform.localScale = koerper.transform.lossyScale;

                koerper.transform.rotation = rot;

                return finalCreature;
            }
            if (creatureContainer == null)
                Debug.LogError("Du hast vergessen, den Container zuzuweisen, Schwachkopf.");

            return koerper;
        }

        private void LateUpdate() {
            addedWirbel = false;
        }

        public IEnumerable<GameObject> getBodyPartList() => koerperTeilListe;
        public void updateText() => textChanged = false;

        public bool hasPlacedVertebra() => addedWirbel;
    }
}