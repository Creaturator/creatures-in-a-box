﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Menüs {
    public class CreditsKnopf : Parent {
        public GameObject untermenu;

        private void Start() => componentName = "Credits Knopf";

        public void ShowCredits() {
            if (!bib.isInMainMenu() || untermenu == null) return;
            Transform menuParent = untermenu.transform.parent;
            GameObject hintergrund = menuParent.Find(bib.getSubMenuBackgroundName()).gameObject;
            if (hintergrund.GetComponent<Image>().enabled)
                for (int i = 0; i < menuParent.childCount; i++) {
                    Transform kategorie = menuParent.GetChild(i);
                    if (kategorie.childCount <= 0) continue;
                    if (kategorie.GetChild(0).gameObject.GetComponent<Behaviour>().enabled)
                        bib.changeComponentStati(kategorie, false);
                }
            
            Text uberschrift = untermenu.transform.parent.Find("Uberschrift").gameObject.GetComponent<Text>();
            hintergrund.GetComponent<Image>().enabled = true;
            bib.changeComponentStati(untermenu, true);
            uberschrift.text = "Credits for UI Images and other Assets";
            uberschrift.enabled = true;

            bib.playRandomSound();
        }
    }
}