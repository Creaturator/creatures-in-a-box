﻿using System;
using System.Diagnostics.Contracts;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;
using Utils;

namespace Menüs {
    public class KopiePasta : Parent {

        public Text toCopy;
        private string normalText;
        private Timer timer;

        private void Start() {
            if (toCopy == null)
                toCopy = gameObject.transform.Find("Text").gameObject.GetComponent<Text>();
            componentName = "Copy Paste Button von " + toCopy.gameObject.name;
            normalText = toCopy.text;
            timer = new Timer();
        }

        public void copyToClipBoard() {
            if (toCopy == null){
                Debug.LogError("Nichts zum Kopieren zugewiesen!");
                return;
            }
            TextEditor copy = new TextEditor {text = toCopy.text.Substring(toCopy.text.IndexOf("http", StringComparison.CurrentCulture))};
            copy.SelectAll();
            copy.Copy();
            toCopy.text = copy.CanPaste() ? "Copied" : "Failed to copy";
            timer.startTimer(1.7f);
        }

        private void Update() {
            if (!timer.isRunning() && toCopy.text.Equals("Copied"))
                toCopy.text = normalText;
        }
    }
}