﻿using UnityEngine.UI;
using Utils;

namespace Menüs {
    public class ComingSoonKnopf : Parent {

        private Timer timer;
        private Text textObj;
        private string eigentlichText;

        private void Start() {
            componentName = "Coming Soon Knopf";
            timer = new Timer();
            textObj = gameObject.transform.Find("Text").gameObject.GetComponent<Text>();
            eigentlichText = textObj.text;
        }

        public void ComingSoonClick() {
            textObj.text = "Coming Soon";
            timer.startTimer(2f);
            bib.playRandomSound();
        }

        public void Update() {
            if (!timer.isRunning() && textObj.text.Equals("Coming Soon"))
                textObj.text = eigentlichText;
        }
    }
}