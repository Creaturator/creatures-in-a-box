﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;
using Utils;

namespace Menüs {
    public class VideoKnopf : Parent {
        private void Start() => componentName = "Video Knopf";

        public void skipTutorial() {
            gameObject.transform.parent.Find("Tutorial Video").gameObject.GetComponent<VideoPlayer>().Stop();
            bib.playRandomSound();
            gameObject.transform.Find("Text").gameObject.GetComponent<Text>().text = "Loading game";
            SceneManager.LoadScene(bib.getLevelSceneName(2));
        }
    }
}