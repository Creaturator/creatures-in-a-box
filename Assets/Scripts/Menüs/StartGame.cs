﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace Menüs {
    public class StartGame : Parent {
        private void Start() => componentName = "Hauptmenu Playscript";

        public void startGame() {
            gameObject.transform.Find("Text").gameObject.GetComponent<Text>().text = "Loading game";
            bib.playRandomSound();
            SceneManager.LoadScene(bib.getLevelSceneName(1));
        }
    }
}