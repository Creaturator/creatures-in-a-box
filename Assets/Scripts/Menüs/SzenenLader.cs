﻿using System.Linq;
using TheEditor.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace Menüs {
    public class SzenenLader : Parent {

        public Text ladeText;
        public Image levelBildObjekt;
        private bool isLoading;
        private bool isWaiting;

        private void Start() {
            componentName = "Szenen Lader";
            isLoading = false;
            isWaiting = false;
        }

        private void LateUpdate() {
            if (isWaiting)
                goto Bild_laden;
            if (isLoading)
                return;
            SceneManager.LoadScene(bib.selectedLevel, LoadSceneMode.Additive);
            isWaiting = true;
            return;

            Bild_laden:
            isWaiting = false;
            Scene levelScene = SceneManager.GetSceneByName(bib.selectedLevel);
            if (!levelScene.GetRootGameObjects().Any(g => g.name.Equals(bib.getLevelObjectName()))) {
                SceneManager.UnloadSceneAsync(levelScene);
                return;
            }
            ladeText.text += bib.selectedLevel;
            RuntimePreviewGenerator.BackgroundColor = Color.clear;
            GameObject level = levelScene.GetRootGameObjects().First(g => g.name.Equals(bib.getLevelObjectName()));
            bib.changeComponentStati(bib.getCanvas().gameObject, false);
            Texture2D levelBild = RuntimePreviewGenerator.GenerateModelPreview(level.transform, Screen.width, Screen.height, true);
            SceneManager.UnloadSceneAsync(levelScene);
            levelBildObjekt.sprite = Sprite.Create(levelBild, Screen.safeArea, Screen.safeArea.center);
            isLoading = true;
            SceneManager.LoadScene(bib.selectedLevel, LoadSceneMode.Single);
        }
    }
}